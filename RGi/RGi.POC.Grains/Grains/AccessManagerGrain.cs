﻿using Orleans;
using Orleans.Providers;
using RGi.POC.GrainInterfaces.Interfaces;
using RGi.POC.Grains.States;
using RGi.POC.OrleansDal;
using RGi.POC.OrleansDal.Common.Interfaces;
using RGi.POC.OrleansDal.DatabaseTypes;
using RGi.POC.OrleansDal.Interfaces;
using RGi.POC_Entities;
using RGi.POC_Models.Requests;
using System;
using System.Security.Authentication;
using System.Threading.Tasks;
using Orleans.Concurrency;

namespace RGi.POC.Grains.Grains
{
    [StorageProvider(ProviderName = "PocGrainStorage")]
    [StatelessWorker(1)]
    public class AccessManagerGrain : Grain, IAccessManager
    {
        private readonly IConnectionStringProvider _connectionStringProvider;
        private readonly IRepositoryFactory _repositoryFactory;

        public AccessManagerGrain(IConnectionStringProvider connectionStringProvider, IRepositoryFactory repositoryFactory)
        {
            _connectionStringProvider = connectionStringProvider;
            _repositoryFactory = repositoryFactory;
        }

        /// <summary>
        /// Log in an unknown player and return the actor for that player.
        /// </summary>
        /// <param name="loginRequest"></param>
        /// <returns></returns>
        public Task<IPlayer> Login(LoginRequest loginRequest)
        {
            try
            {
                var unitOfWorkFactory = new UnitOfWorkFactory(_connectionStringProvider, _repositoryFactory);
                var unitOfWork = unitOfWorkFactory.CreateConnection<RgiDatabase>();

                var player = unitOfWork.PlayerRepository.Login(loginRequest.Username, loginRequest.Password);

                if (player == null)
                {
                    throw new InvalidCredentialException($"Player with these credentials not found.");
                }

                if (!player.IsLoggedIn)
                {
                    player.IsLoggedIn = true;
                    // Update the external system
                    unitOfWork.PlayerRepository.UpdateIsLoggedIn(player.Id, player.IsLoggedIn);
                    unitOfWork.Commit();
                }

                // Create the player grain (it might exist already)
                var playerGrain = GrainFactory.GetGrain<IPlayer>(player.Id);
                playerGrain.SetLoggedin();
                unitOfWork.Dispose();
                return Task.FromResult(playerGrain);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
