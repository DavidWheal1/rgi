﻿using Orleans;
using Orleans.Concurrency;
using Orleans.Providers;
using RGi.POC.GrainInterfaces.Interfaces;
using RGi.POC.OrleansDal;
using RGi.POC.OrleansDal.BusinessLogic;
using RGi.POC.OrleansDal.Common.Interfaces;
using RGi.POC.OrleansDal.DatabaseTypes;
using RGi.POC.OrleansDal.Interfaces;
using RGi.POC_Models.Requests;
using Serilog;
using System.Threading.Tasks;


namespace RGi.POC.Grains.Grains
{
    [StorageProvider(ProviderName = "PocGrainStorage")]
    [StatelessWorker(1)]
    public class TransactionManagerGrain : Grain, ITransactionManager
    {
        private readonly IConnectionStringProvider _connectionStringProvider;
        private readonly IRepositoryFactory _repositoryFactory;
        private readonly ILogger _logger;

        public TransactionManagerGrain(IConnectionStringProvider connectionStringProvider, IRepositoryFactory repositoryFactory)
        {
            _connectionStringProvider = connectionStringProvider;
            _repositoryFactory = repositoryFactory;
            _logger = new LoggerConfiguration().WriteTo.RollingFile(@"C:\temp\RGi.POC-Orleans-CreateTransaction-{Date}.txt").CreateLogger();
        }

        public Task<int> Credit(TransactionRequest transactionRequest)
        {
            var unitOfWorkFactory = new UnitOfWorkFactory(_connectionStringProvider, _repositoryFactory);
            var unitOfWork = unitOfWorkFactory.CreateConnection<RgiDatabase>();

            var res = Task.FromResult(TransactionHelper.CreateTransaction(transactionRequest, unitOfWork, _logger));
            unitOfWork.Dispose();
            return res;
        }

        public Task<int> Debit(TransactionRequest transactionRequest)
        {
            var unitOfWorkFactory = new UnitOfWorkFactory(_connectionStringProvider, _repositoryFactory);
            var unitOfWork = unitOfWorkFactory.CreateConnection<RgiDatabase>();

            var res = Task.FromResult(TransactionHelper.CreateTransaction(transactionRequest, unitOfWork, _logger));
            unitOfWork.Dispose();
            return res;
        }


    }
}