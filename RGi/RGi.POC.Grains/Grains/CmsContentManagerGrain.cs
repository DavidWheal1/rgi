﻿using Orleans;
using Orleans.Providers;
using RGi.POC.GrainInterfaces.Interfaces;
using RGi.POC.Grains.States;
using RGi.POC.OrleansDal;
using RGi.POC.OrleansDal.Common.Interfaces;
using RGi.POC.OrleansDal.DatabaseTypes;
using RGi.POC.OrleansDal.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RGi.POC_Entities;

namespace RGi.POC.Grains.Grains
{
    [StorageProvider(ProviderName = "PocGrainStorage")]
    public class CmsContentManagerGrain : Grain<CmsState>, ICmsContentManager
    {
        private readonly IConnectionStringProvider _connectionStringProvider;
        private readonly IRepositoryFactory _repositoryFactory;

        public CmsContentManagerGrain(IConnectionStringProvider connectionStringProvider, IRepositoryFactory repositoryFactory)
        {
            _connectionStringProvider = connectionStringProvider;
            _repositoryFactory = repositoryFactory;
        }

        public override async Task OnActivateAsync()
        {
            var unitOfWorkFactory = new UnitOfWorkFactory(_connectionStringProvider, _repositoryFactory);
            var unitOfWork = unitOfWorkFactory.CreateConnection<RgiDatabase>();
            var games = unitOfWork.GameRepository.Get();
            State.Games = games.ToList();

            foreach (var game in State.Games)

            {
                // Create the game grains
                GrainFactory.GetGrain<IGame>(game.Id);
            }
            unitOfWork.Dispose();

            await base.WriteStateAsync();
            await base.OnActivateAsync();
        }

        public Task<Game> Get(int gameId)
        {
            return Task.FromResult(State.Games.FirstOrDefault(x => x.Id == gameId));
        }

        public Task<List<Game>> List()
        {
            return Task.FromResult(State.Games);
        }
    }
}