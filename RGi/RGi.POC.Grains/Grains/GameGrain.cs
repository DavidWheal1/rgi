﻿using Orleans;
using Orleans.Providers;
using RGi.POC.GrainInterfaces.Interfaces;
using RGi.POC.Grains.States;
using RGi.POC.OrleansDal;
using RGi.POC.OrleansDal.Common.Interfaces;
using RGi.POC.OrleansDal.DatabaseTypes;
using RGi.POC.OrleansDal.Interfaces;
using System.Threading.Tasks;

namespace RGi.POC.Grains.Grains
{
    [StorageProvider(ProviderName = "PocGrainStorage")]
    public class GameGrain : Grain<GameState>, IGame
    {
        private readonly IConnectionStringProvider _connectionStringProvider;
        private readonly IRepositoryFactory _repositoryFactory;

        public GameGrain(IConnectionStringProvider connectionStringProvider, IRepositoryFactory repositoryFactory)
        {
            _connectionStringProvider = connectionStringProvider;
            _repositoryFactory = repositoryFactory;
        }
        public override async Task OnActivateAsync()
        {
            var unitOfWorkFactory = new UnitOfWorkFactory(_connectionStringProvider, _repositoryFactory);
            var unitOfWork = unitOfWorkFactory.CreateConnection<RgiDatabase>();

            State.Game = unitOfWork.GameRepository.Get((int)this.GetPrimaryKeyLong());
            await base.WriteStateAsync();
            await base.OnActivateAsync();
            unitOfWork.Dispose();

        }
    }
}