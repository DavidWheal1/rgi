﻿using Orleans;
using Orleans.Providers;
using RGi.POC.GrainInterfaces.Interfaces;
using RGi.POC.Grains.States;
using RGi.POC.OrleansDal;
using RGi.POC.OrleansDal.Common.Interfaces;
using RGi.POC.OrleansDal.DatabaseTypes;
using RGi.POC.OrleansDal.Interfaces;
using RGi.POC_Entities;
using System;
using System.Threading.Tasks;
using static RGi.POC.Grains.States.GameSessionState;

namespace RGi.POC.Grains.Grains
{
    [StorageProvider(ProviderName = "PocGrainStorage")]
    public class GameSessionGrain : Grain<GameSessionState>, IGameSession
    {
        private int _playerId;
        private int _gameId;
        private readonly IConnectionStringProvider _connectionStringProvider;
        private readonly IRepositoryFactory _repositoryFactory;
        private IPlayer _player;
        private string _gameIdKey;

        public GameSessionGrain(IConnectionStringProvider connectionStringProvider, IRepositoryFactory repositoryFactory)
        {
            _connectionStringProvider = connectionStringProvider;
            _repositoryFactory = repositoryFactory;
        }

        public override Task OnActivateAsync()
        {
            _playerId = (int)this.GetPrimaryKeyLong(out _gameIdKey);
            _player = GrainFactory.GetGrain<IPlayer>(_playerId);
            return base.OnActivateAsync();
        }

        public async Task<int> Start(Guid token)
        {
            if (await _player.IsLoggedIn())
            {
                // Update the legacy database for BI etc
                int newId = 0;
                var unitOfWorkFactory = new UnitOfWorkFactory(_connectionStringProvider, _repositoryFactory);
                var unitOfWork = unitOfWorkFactory.CreateConnection<RgiDatabase>();

                _gameId = Convert.ToInt32(_gameIdKey);

                var game = GrainFactory.GetGrain<IGame>(_gameId);

                if (game != null)
                {
                    var gameSession = new GameSession
                    {
                        GameId = _gameId,
                        PlayerId = _playerId,
                        PlayerInSession = true,
                        Token = token.ToString()
                    };

                    newId = unitOfWork.GameSessionRepository.Add(gameSession);
                    unitOfWork.Commit();

                    // Now keep our internal state updated
                    if (!State.Sessions.ContainsKey(newId))
                    {
                        var gameInfo = new GameInfo
                        {
                            GameId = _gameId,
                            StartDate = DateTime.UtcNow,
                            Token = token
                        };

                        State.Sessions.Add(newId, gameInfo);
                        await base.WriteStateAsync();
                    }
                    unitOfWork.Dispose();

                    return newId;
                }
                unitOfWork.Dispose();

            }

            return -1;
        }

        public async Task Stop(int gameSessionId)
        {
            if (await _player.IsLoggedIn())
            {
                var unitOfWorkFactory = new UnitOfWorkFactory(_connectionStringProvider, _repositoryFactory);
                var unitOfWork = unitOfWorkFactory.CreateConnection<RgiDatabase>();

                _playerId = (int)this.GetPrimaryKeyLong(out string gameIdKey);
                _gameId = Convert.ToInt32(gameIdKey);

                var game = GrainFactory.GetGrain<IGame>(_gameId);

                if (game != null)
                {
                    var gameSession = unitOfWork.GameSessionRepository.Get(gameSessionId);

                    if (gameSession != null)
                    {
                        gameSession.PlayerInSession = false;
                        gameSession.Token = null;
                        gameSession.Updated = DateTime.UtcNow;
                        unitOfWork.GameSessionRepository.Update(gameSession);
                        unitOfWork.Commit();

                        // Now keep our internal state updated
                        State.Sessions.Remove(gameSessionId);
                        await base.WriteStateAsync();
                    }
                }
                unitOfWork.Dispose();

            }
        }
    }
}