﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Providers;
using Orleans.Storage;
using RGi.POC.GrainInterfaces.Interfaces;
using RGi.POC.Grains.States;

namespace RGi.POC.Grains.Grains
{
    /// <summary>
    /// Orleans grain implementation class HelloGrain.
    /// </summary>
    [StorageProvider(ProviderName = "PocGrainStorage")]
    public class HelloGrain : Orleans.Grain<HelloState>, IHello
    {
        private readonly ILogger logger;

        public HelloGrain(ILogger<HelloGrain> logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Test the state
        /// </summary>
        /// <param name="greeting"></param>
        /// <returns></returns>
        async Task<string> IHello.SayHello(string greeting)
        {
            string res = string.Empty;

            await base.ReadStateAsync();
            res = $"State count is {State.Count}";

            //base.DeactivateOnIdle();    // Call  after a fail so that state will get read again and reinitialised

            State.Hello = greeting;
            CpuIntensive();             // Load for a perf test
            State.Count++;
            await base.WriteStateAsync();
            return $"Hi, I'm working ok. {res} from {this.GetPrimaryKeyLong()}";
        }

        public async Task Reset()
        {
            State.Count = 0;
            State.Hello = "";
            await base.WriteStateAsync();
        }

        /// <summary>
        /// Generate a set of random numbers
        /// </summary>
        void CpuIntensive()
        {
            var rand = new Random();
            for (int i = 0; i < 100; i++)
            {
                var rn = rand.Next();
            }
        }

        /// <summary>
        /// Called when the state is read initially
        /// </summary>
        /// <returns></returns>
        public override Task OnActivateAsync()
        {

            return base.OnActivateAsync();
        }
    }
}
