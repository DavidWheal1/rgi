﻿using Orleans;
using Orleans.Providers;
using RGi.POC.GrainInterfaces.Interfaces;
using RGi.POC.Grains.States;
using RGi.POC_Models.Enums;
using RGi.POC_Models.Requests;
using System;
using System.Threading.Tasks;

namespace RGi.POC.Grains.Grains
{
    [StorageProvider(ProviderName = "PocGrainStorage")]

    public class RealityCheckGrain : Grain<RealityCheckState>, IRealityCheck
    {
        private IPlayer _player;
        private const int _realityCheckIntervalMinutes = 20;

        public override Task OnActivateAsync()
        {
            _player = GrainFactory.GetGrain<IPlayer>(this.GetPrimaryKeyLong());
            return base.OnActivateAsync();
        }

        public Task Setup()
        {
            throw new NotImplementedException();
        }

        public async Task StartGame(int gameId)
        {
            if (await _player.IsLoggedIn())
            {
                State.GamesInPlay++;
                if (State.StartPeriodUtc == null)
                {
                    State.StartPeriodUtc = DateTime.UtcNow;
                }

                await WriteStateAsync();
            }
        }

        public async Task StopGame(int gameId)
        {
            if (await _player.IsLoggedIn())
            {
                State.GamesInPlay--;
                if (State.GamesInPlay == 0)
                {
                    State.StartPeriodUtc = null;
                }

                await WriteStateAsync();
            }
        }

        public async Task<RealityCheckCheckEnum> Check()
        {
            if (await _player.IsLoggedIn())
            {
                if (State.StartPeriodUtc != null)
                {
                    var ta = DateTime.UtcNow.Subtract((DateTime)State.StartPeriodUtc);

                    return ta.TotalMinutes > _realityCheckIntervalMinutes ? RealityCheckCheckEnum.Expired : RealityCheckCheckEnum.Ok;
                }
            }

            return RealityCheckCheckEnum.Unknown;
        }

        public async Task Result(RealityCheckResultRequest realityCheckResultRequest)
        {
            if (await _player.IsLoggedIn())
            {
                if (realityCheckResultRequest.RealityCheckResult == RealityCheckResultEnum.Stop)
                {
                    State.GamesInPlay = 0;
                    State.StartPeriodUtc = null;
                }
                else
                {
                    State.StartPeriodUtc = DateTime.UtcNow;
                }

                await WriteStateAsync();
            }
        }
    }
}