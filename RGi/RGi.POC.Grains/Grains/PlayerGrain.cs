﻿using Orleans;
using Orleans.Providers;
using RGi.POC.GrainInterfaces.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RGi.POC.Grains.States;
using RGi.POC.OrleansDal.Common.Interfaces;
using RGi.POC.OrleansDal.Interfaces;
using RGi.POC.OrleansDal;
using RGi.POC.OrleansDal.DatabaseTypes;
using RGi.POC_Entities;
using RGi.POC_Models.Requests;


namespace RGi.POC.Grains.Grains
{
    [StorageProvider(ProviderName = "PocGrainStorage")]
    public class PlayerGrain : Grain<PlayerState>, IPlayer
    {
        private readonly IConnectionStringProvider _connectionStringProvider;
        private readonly IRepositoryFactory _repositoryFactory;

        public PlayerGrain(IConnectionStringProvider connectionStringProvider, IRepositoryFactory repositoryFactory)
        {
            _connectionStringProvider = connectionStringProvider;
            _repositoryFactory = repositoryFactory;
        }

        /// <summary>
        /// Read state from the legacy database on start up
        /// </summary>
        /// <returns></returns>
        public override async Task OnActivateAsync()
        {
            var unitOfWorkFactory = new UnitOfWorkFactory(_connectionStringProvider, _repositoryFactory);
            var unitOfWork = unitOfWorkFactory.CreateConnection<RgiDatabase>();
            var me = unitOfWork.PlayerRepository.Get((int)this.GetPrimaryKeyLong());

            State.Me = me ?? throw new KeyNotFoundException();
            State.MyRealityCheck = GrainFactory.GetGrain<IRealityCheck>(State.Me.Id);
            await base.WriteStateAsync();
            await base.OnActivateAsync();
            unitOfWork.Dispose();

        }

        /// <summary>
        /// Only look at the grain state for the flag
        /// </summary>
        /// <returns></returns>
        public Task<bool> IsLoggedIn()
        {
            return Task.FromResult(State.IsLoggedIn);
        }

        /// <summary>
        /// Get a list of games I can play from the Cms grain
        /// </summary>
        /// <returns></returns>
        public Task<List<Game>> GetGamesList()
        {
            var cmsContentManager = GrainFactory.GetGrain<ICmsContentManager>(Guid.Empty);

            return cmsContentManager.List();
        }

        /// <summary>
        /// Set my state to be logged in, do not update the legacy database
        /// </summary>
        /// <returns></returns>
        public Task<bool> SetLoggedin()
        {
            State.IsLoggedIn = true;
            base.WriteStateAsync();
            return Task.FromResult(true);
        }

        /// <summary>
        /// Logout and keep the state and legacy database in sync
        /// </summary>
        /// <returns></returns>
        public Task Logout()
        {
            if (!State.IsLoggedIn)
            {
                return Task.CompletedTask;
            }

            var unitOfWorkFactory = new UnitOfWorkFactory(_connectionStringProvider, _repositoryFactory);
            var unitOfWork = unitOfWorkFactory.CreateConnection<RgiDatabase>();

            unitOfWork.PlayerRepository.UpdateIsLoggedIn(State.Me.Id, false);
            State.IsLoggedIn = false;
            base.WriteStateAsync();
            unitOfWork.Commit();
            unitOfWork.Dispose();

            return Task.CompletedTask;
        }

        public Task<decimal> Balance(int playerId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Updates the game session table
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="isExternal"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<int> StartGame(int gameId, bool isExternal, Guid token)
        {
            int res = -1;

            if (State.IsLoggedIn)
            {
                var game = GrainFactory.GetGrain<IGame>(gameId);

                if (game != null)
                {
                    var session = GrainFactory.GetGrain<IGameSession>(State.Me.Id, gameId.ToString(), null);

                    if (session != null)
                    {
                        res = await session.Start(token);
                        await State.MyRealityCheck.StartGame(gameId);
                    }
                }
            }

            return res;
        }

        /// <summary>
        /// Updates the game session table
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task StopGame(int gameId, int gameSessionId)
        {
            if (State.IsLoggedIn)
            {
                var game = GrainFactory.GetGrain<IGame>(gameId);

                if (game != null)
                {
                    var session = GrainFactory.GetGrain<IGameSession>(State.Me.Id, gameId.ToString(), null);

                    if (session != null)
                    {
                        await session.Stop(gameSessionId);
                        await State.MyRealityCheck.StopGame(gameId);
                    }
                }
            }
        }

        public async Task<int> Win(int gameSessionId, int gameId, decimal amount)
        {
            if (State.IsLoggedIn)
            {
                var gameSession = GrainFactory.GetGrain<IGameSession>(State.Me.Id, gameId.ToString(), null);

                if (gameSession != null)
                {
                    var transactionManager = GrainFactory.GetGrain<ITransactionManager>(Guid.Empty);

                    return await transactionManager.Credit(new TransactionRequest()
                    {
                        GameId = gameId,
                        GameSessionId = gameSessionId,
                        PlayerId = (int)this.GetPrimaryKeyLong(),
                        Amount = amount
                    });
                }
            }

            return -1;
        }

        public async Task<int> Lose(int gameSessionId, int gameId, decimal amount)
        {
            if (State.IsLoggedIn)
            {
                var session = GrainFactory.GetGrain<IGameSession>(State.Me.Id, gameId.ToString(), null);

                if (session != null)
                {
                    var transactionManager = GrainFactory.GetGrain<ITransactionManager>(Guid.Empty);

                    // Game session Id is irrelevant as it is in a grain state ??
                    return await transactionManager.Debit(new TransactionRequest()
                    {
                        GameId = gameId,
                        GameSessionId = gameSessionId,
                        PlayerId = (int)this.GetPrimaryKeyLong(),
                        Amount = amount
                    });
                }
            }

            return -1;
        }

    }
}