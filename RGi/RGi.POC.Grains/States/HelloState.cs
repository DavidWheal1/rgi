﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RGi.POC.Grains.States
{
    public class HelloState
    {
        public string Hello { get; set; }
        public int Count { get; set; }
    }
}
