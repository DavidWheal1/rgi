﻿using System;
using System.Collections.Generic;

namespace RGi.POC.Grains.States
{
    public class GameSessionState
    {
        public class GameInfo
        {
            public int GameId { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime? StopDate { get; set; }
            public Guid Token { get; set; }
        }

        public IDictionary<int, GameInfo> Sessions { get; } = new Dictionary<int, GameInfo>();
    }
}