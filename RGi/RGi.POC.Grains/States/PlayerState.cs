﻿using System;
using System.Collections.Generic;
using System.Text;
using RGi.POC.GrainInterfaces.Interfaces;
using RGi.POC_Entities;

namespace RGi.POC.Grains.States
{
    public class PlayerState
    {
        public Player Me { get; set; }
        public bool IsLoggedIn { get; set; }
        public IRealityCheck MyRealityCheck { get; set; }
    }
}
