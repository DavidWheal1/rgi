﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RGi.POC.Grains.States
{
    public class RealityCheckState
    {
        public DateTime? StartPeriodUtc { get; set; }
        public int GamesInPlay { get; set; }
    }
}
