﻿namespace RGi.POC.Config
{
    /// <summary>
    /// TODO: Make this read app.config etc
    /// </summary>
    public class SiloConfiguration
    {
        public string ClusterId { get; set; }
        public string ServiceId { get; set; }
        public string ClusterProviderConnectionString { get; set; }
        public string ClusterProviderInvariant { get; set; }
        public string GrainStorageName { get; set; }

        public SiloConfiguration()
        {
            ClusterId = 
            ServiceId = "POCRGi";
            // MySql cluster provider seems not to work and fails with an invalid cast
            ClusterProviderConnectionString =
                "server=poc-orleans.c1iqx3klgoev.eu-west-2.rds.amazonaws.com;database=pocorleans;user=orleans;password=a7ZcLQXCnDhp;Max Pool Size=10;Connection Lifetime=50;charset=utf8";
            ClusterProviderInvariant = "MySql.Data.MySqlClient";
            GrainStorageName = "PocGrainStorage";
        }
    }
}
