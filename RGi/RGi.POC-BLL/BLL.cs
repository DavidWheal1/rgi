﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace RGi.POC_BLL
{
    public class BLL
    {
        public static readonly int _defaultPort = 64454;

        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseUrls($"http://*:{GetPortConfigurationFromFile()}")
                .Build();

        public static int GetPortConfigurationFromFile()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appSettings.json", false, false)
                .Build();
            string portString = configuration["webApiPort"];

            if (string.IsNullOrWhiteSpace(portString) || !int.TryParse(portString, out int port))
            {
                return _defaultPort;
            }

            return port;
        }
    }
}