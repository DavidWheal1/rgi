﻿using Microsoft.AspNetCore.Mvc;
using RGi.POC_DAL.Common.Interfaces;
using RGi.POC_DAL.Interfaces;
using RGi.POC_Entities;
using RGi.POC_Models.Requests;

namespace RGi.POC_BLL.Controllers
{
    [Produces("application/json")]
    [Route("api/bl_player")]
    public class PlayerController : BaseController
    {

        public PlayerController(IConnectionStringProvider connectionStringProvider, IRepositoryFactory repositoryFactory) : base(connectionStringProvider, repositoryFactory)
        {
        }

        [HttpPost("login")]
        public Player Login([FromBody]LoginRequest loginRequest)
        {
            var player = UnitOfWork.PlayerRepository.Login(loginRequest.Username, loginRequest.Password);

            if (player != null && !player.IsLoggedIn)
            {
                player.IsLoggedIn = true;
                UnitOfWork.PlayerRepository.UpdateIsLoggedIn(player.Id, player.IsLoggedIn);
                UnitOfWork.Commit();
            }
            UnitOfWork.Dispose();
            return player;
        }

        [HttpDelete("logout/{playerId:int}")]
        public void Logout(int playerId)
        {
            var player = UnitOfWork.PlayerRepository.Get(playerId);

            if (player != null && player.IsLoggedIn)
            {
                player.IsLoggedIn = false;
                UnitOfWork.PlayerRepository.UpdateIsLoggedIn(playerId, player.IsLoggedIn);
                UnitOfWork.Commit();
            }
            UnitOfWork.Dispose();
        }
    }
}