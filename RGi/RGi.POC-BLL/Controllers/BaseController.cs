﻿using Microsoft.AspNetCore.Mvc;
using RGi.POC_DAL;
using RGi.POC_DAL.Common;
using RGi.POC_DAL.Common.Interfaces;
using RGi.POC_DAL.DatabaseTypes;
using RGi.POC_DAL.Interfaces;
using Serilog;


namespace RGi.POC_BLL.Controllers
{
    public class BaseController : Controller
    {
        private IConnectionStringProvider _connectionStringProvider;
        private IRepositoryFactory _repositoryFactory ;
        private IUnitOfWorkFactory _unitOfWorkFactory;
        protected IUnitOfWork UnitOfWork;
        protected ILogger Logger;


        public BaseController(IConnectionStringProvider connectionStringProvider,IRepositoryFactory repositoryFactory)
        {
            _connectionStringProvider = connectionStringProvider;
            _repositoryFactory = repositoryFactory;
            _unitOfWorkFactory = new UnitOfWorkFactory(_connectionStringProvider, _repositoryFactory);
            UnitOfWork = _unitOfWorkFactory.CreateConnection<RgiDatabase>();            
            Logger = new LoggerConfiguration().WriteTo.RollingFile(@"C:\temp\RGi.POC-BLL-{Date}.txt").CreateLogger();

        }
    }
}