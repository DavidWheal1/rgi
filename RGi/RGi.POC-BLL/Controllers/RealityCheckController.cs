﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RGi.POC_Models.Enums;
using RGi.POC_Models.Requests;
using System;
using System.IO;
using System.Linq;
using RGi.POC_DAL.Common.Interfaces;
using RGi.POC_DAL.Interfaces;

namespace RGi.POC_BLL.Controllers
{
    [Produces("application/json")]
    [Route("api/bl_realitycheck")]
    public class RealityCheckController : BaseController
    {

        public RealityCheckController(IConnectionStringProvider connectionStringProvider, IRepositoryFactory repositoryFactory) : base(connectionStringProvider, repositoryFactory)
        {
        }


        [HttpGet("check/{playerId:int}")]
        public RealityCheckCheckEnum RealityCheckCheck(int playerId)
        {
            var player = UnitOfWork.PlayerRepository.Get(playerId);

            if (player != null && player.IsLoggedIn)
            {
                var realityCheckList = UnitOfWork.RealityCheckRepository.Get(playerId);

                if (realityCheckList.Any())
                {
                    var configuration = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appSettings.json", false, false)
                        .Build();
                    double realityCheckIntervalMinutes = Convert.ToDouble(configuration["realityCheckIntervalMinutes"]);
                    var realityCheck = realityCheckList.Single(rc => rc.Timer != null);
                    var timePlaying = DateTime.UtcNow.AddTicks(-realityCheck.Timer.Value).Ticks;

                    // TODO create config variable for reality check timespan
                    return TimeSpan.FromTicks(timePlaying).TotalMinutes >= realityCheckIntervalMinutes ? RealityCheckCheckEnum.Expired : RealityCheckCheckEnum.Ok;
                }
            }
            UnitOfWork.Dispose();
            return RealityCheckCheckEnum.Unknown;
        }

        [HttpPut("result")]
        public void RealityCheckResult([FromBody]RealityCheckResultRequest realityCheckResultRequest)
        {
            var player = UnitOfWork.PlayerRepository.Get(realityCheckResultRequest.PlayerId);

            if (player != null && player.IsLoggedIn)
            {
                var realityCheckList = UnitOfWork.RealityCheckRepository.Get(realityCheckResultRequest.PlayerId);

                if (realityCheckList.Any())
                {
                    var realityCheck = realityCheckList.First(rc => rc.Timer != null);

                    realityCheck.Timer = DateTime.UtcNow.Ticks;
                    UnitOfWork.RealityCheckRepository.UpdateTimer(realityCheck);
                    if (realityCheckResultRequest.RealityCheckResult == RealityCheckResultEnum.Stop)
                    {
                        UnitOfWork.RealityCheckRepository.Remove(realityCheckResultRequest.PlayerId);
                    }

                    UnitOfWork.Commit();
                }
            }
            UnitOfWork.Dispose();
        }
    }
}