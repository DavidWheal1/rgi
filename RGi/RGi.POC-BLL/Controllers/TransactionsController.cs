﻿using Microsoft.AspNetCore.Mvc;
using RGi.POC_DAL.Common.Interfaces;
using RGi.POC_DAL.Interfaces;
using RGi.POC_Entities;
using RGi.POC_Models.Requests;
using Serilog;

namespace RGi.POC_BLL.Controllers
{
    [Produces("application/json")]
    [Route("api/bl_transactions")]
    public class TransactionsController : BaseController
    {

        public TransactionsController(IConnectionStringProvider connectionStringProvider, IRepositoryFactory repositoryFactory)
            : base(connectionStringProvider, repositoryFactory)
        {

        }


        [HttpPost("debit")]
        public int Stake([FromBody]TransactionRequest transactionRequest)
        {
            return CreateTransaction(transactionRequest);
        }

        [HttpPost("credit")]
        public int Win([FromBody]TransactionRequest transactionRequest)
        {
            return CreateTransaction(transactionRequest);
        }

        private int CreateTransaction(TransactionRequest transactionRequest)
        {
            var player = UnitOfWork.PlayerRepository.Get(transactionRequest.PlayerId);

            if (player != null && player.IsLoggedIn)
            {
                var game = UnitOfWork.GameRepository.Get(transactionRequest.GameId);

                if (game != null)
                {
                    var gameSession = UnitOfWork.GameSessionRepository.Get(transactionRequest.GameSessionId);

                    if (gameSession != null)
                    {
                        var transaction = new Transaction
                        {
                            Amount = transactionRequest.Amount,
                            GameSessionId = gameSession.Id,
                            WalletId = 1 // hardcoded, for now
                        };
                        Logger.Warning($"Writing transaction");
                        var transactionId = UnitOfWork.TransactionRepository.Add(transaction);

                        UnitOfWork.Commit();
                        UnitOfWork.Dispose();
                        return transactionId;
                    }
                    else
                    {
                        Logger.Warning($"NOT 1 Writing transaction");

                    }
                }
                else
                {
                    Logger.Warning($"NOT 2 Writing transaction");
                    
                }
            }
            else
            {
                Logger.Warning($"NOT 3 Writing transaction");

            }

            return -1;
        }
    }
}