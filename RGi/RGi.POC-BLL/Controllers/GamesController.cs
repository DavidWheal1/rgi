﻿using Microsoft.AspNetCore.Mvc;
using RGi.POC_Entities;
using System.Collections.Generic;
using RGi.POC_DAL.Common.Interfaces;
using RGi.POC_DAL.Interfaces;

namespace RGi.POC_BLL.Controllers
{
    [Produces("application/json")]
    [Route("api/bl_games")]
    public class GamesController : BaseController
    {
        public GamesController(IConnectionStringProvider connectionStringProvider, IRepositoryFactory repositoryFactory)
            : base(connectionStringProvider, repositoryFactory)
        {
        }

        [HttpGet(Name = "GetGamesList")]
        public IEnumerable<Game> Get()
        {
            var res =  UnitOfWork.GameRepository.Get();
            UnitOfWork.Dispose();
            return res;
        }

        [HttpGet("{gameId:int}", Name = "GetGame")]
        public Game Get(int gameId)
        {
            var res = UnitOfWork.GameRepository.Get(gameId);
            UnitOfWork.Dispose();
            return res;
        }
    }
}