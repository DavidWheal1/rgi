﻿using Microsoft.AspNetCore.Mvc;
using RGi.POC_Entities;
using RGi.POC_Models.Requests;
using System;
using System.Linq;
using RGi.POC_DAL.Common.Interfaces;
using RGi.POC_DAL.Interfaces;

namespace RGi.POC_BLL.Controllers
{
    [Produces("application/json")]
    [Route("api/bl_gamesession")]
    public class GameSessionController : BaseController
    {
        public GameSessionController(IConnectionStringProvider connectionStringProvider, IRepositoryFactory repositoryFactory) : base(connectionStringProvider, repositoryFactory)
        {
        }

        [HttpPost("start")]
        public int StartPlay([FromBody]GameSessionStartRequest gameSessionRequest)
        {
            var player = UnitOfWork.PlayerRepository.Get(gameSessionRequest.PlayerId);

            if (player != null && player.IsLoggedIn)
            {
                var game = UnitOfWork.GameRepository.Get(gameSessionRequest.GameId);

                if (game != null)
                {
                    var gameSession = new GameSession
                    {
                        GameId = gameSessionRequest.GameId,
                        PlayerId = gameSessionRequest.PlayerId,
                        PlayerInSession = true,
                        Token = Guid.NewGuid().ToString()
                    };
                    var gameSessionId = UnitOfWork.GameSessionRepository.Add(gameSession);

                    AddGameToRealityCheck(gameSessionRequest.PlayerId, gameSessionRequest.GameId);
                    UnitOfWork.Commit();
                    UnitOfWork.Dispose();
                    return gameSessionId;
                }
            }

            return -1;
        }

        [HttpPut("stop")]
        public void StopPlay([FromBody]GameSessionStopRequest gameSessionRequest)
        {
            var player = UnitOfWork.PlayerRepository.Get(gameSessionRequest.PlayerId);

            if (player != null && player.IsLoggedIn)
            {
                var gameSession = UnitOfWork.GameSessionRepository.Get(gameSessionRequest.GameSessionId);

                if (gameSession != null)
                {
                    gameSession.PlayerInSession = false;
                    gameSession.Token = null;
                    gameSession.Updated = DateTime.UtcNow;
                    UnitOfWork.GameSessionRepository.Update(gameSession);
                    RemoveGameFromRealityCheck(gameSessionRequest.PlayerId, gameSessionRequest.GameId);
                    UnitOfWork.Commit();
                    UnitOfWork.Dispose();
                }
            }
        }

        private void AddGameToRealityCheck(int playerId, int gameId)
        {
            var realityCheckList = UnitOfWork.RealityCheckRepository.Get(playerId);

            if (!realityCheckList.Any())
            {
                var realityCheck = new RealityCheck
                {
                    PlayerId = playerId,
                    GameId = gameId,
                    Timer = DateTime.UtcNow.Ticks
                };

                UnitOfWork.RealityCheckRepository.Add(realityCheck);
            }
            else
            {
                var realityCheck = realityCheckList.SingleOrDefault(rc => rc.GameId == gameId);

                if (realityCheck == null)
                {
                    var realityCheckTimer = realityCheckList.First(rc => rc.Timer.HasValue);
                    realityCheck = new RealityCheck
                    {
                        PlayerId = playerId,
                        GameId = gameId,
                        Timer = realityCheckTimer.Timer
                    };
                    UnitOfWork.RealityCheckRepository.Add(realityCheck);
                }
            }
        }

        private void RemoveGameFromRealityCheck(int playerId, int gameId)
        {
            var realityCheckList = UnitOfWork.RealityCheckRepository.Get(playerId);

            if (realityCheckList != null && realityCheckList.Any())
            {
                var realityCheck = realityCheckList.SingleOrDefault(rc => rc.GameId == gameId);

                if (realityCheck != null)
                {
                    UnitOfWork.RealityCheckRepository.Remove(realityCheck.PlayerId, realityCheck.GameId);
                }
            }
        }
    }
}