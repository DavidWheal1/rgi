﻿using RGi.POC_Entities;
using System.Collections.Generic;

namespace RGi.POC_DAL.Repositories.Interfaces
{
    public interface IRealityCheckRepository
    {
        void Add(RealityCheck realityCheck);
        IEnumerable<RealityCheck> Get(int playerId);
        void Remove(int playerId, int gameId);
        void Remove(int playerId);
        void UpdateTimer(RealityCheck realityCheck);
    }
}