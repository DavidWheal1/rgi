﻿using RGi.POC_Entities;
using System.Collections.Generic;

namespace RGi.POC_DAL.Repositories.Interfaces
{
    public interface IGameRepository
    {
        void Add(Game game);
        Game Get(int gameId);
        IEnumerable<Game> Get();
    }
}