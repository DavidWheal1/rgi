﻿using RGi.POC_Entities;

namespace RGi.POC_DAL.Repositories.Interfaces
{
    public interface ITransactionRepository
    {
        int Add(Transaction transaction);
        Transaction Get(int transactionId);
    }
}