﻿using Dapper;
using RGi.POC_DAL.Repositories.Interfaces;
using RGi.POC_Entities;
using System.Data;

namespace RGi.POC_DAL.Repositories
{
    public class WalletRepository : BaseRepository, IWalletRepository
    {
        public WalletRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public void Add(Wallet wallet)
        {
            var sql = @"
                INSERT INTO wallet(Name)
                VALUES (@name)";
            var args = new { name = wallet.Name };

            Connection.Execute(sql, args, Transaction);
        }

        public Wallet Get(int walletId)
        {
            var sql = @"
                SELECT Id, Name
                FROM wallets
                WHERE Id = @walletId";
            var args = new { walletId = walletId };

            return Connection.QuerySingleOrDefault<Wallet>(sql, args);
        }
    }
}