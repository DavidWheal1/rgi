﻿using Dapper;
using RGi.POC_DAL.Repositories.Interfaces;
using RGi.POC_Entities;
using System.Data;

namespace RGi.POC_DAL.Repositories
{
    public class PlayerRepository : BaseRepository, IPlayerRepository
    {
        public PlayerRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public void Add(Player player)
        {
            var sql = @"
                INSERT INTO players(Barcode, Created, Username, Password)
                VALUES (@barcode, UTC_TIMESTAMP(), @username, @password)";
            var args = new { barcode = player.Barcode, username = player.Username, password = player.Password };

            Connection.Execute(sql, args, Transaction);
        }

        public Player Get(int playerId)
        {
            var sql = @"
                SELECT Id, Barcode, Created, Username, IsLoggedIn
                FROM players
                WHERE Id = @playerId";
            var args = new { playerId = playerId };

            return Connection.QuerySingleOrDefault<Player>(sql, args);
        }

        public Player Login(string username, string password)
        {
            var sql = @"
                SELECT Id, Barcode, Created, Username, IsLoggedIn
                FROM players
                WHERE Username = @username AND Password = @password";
            var args = new { username = username, password = password };

            return Connection.QuerySingleOrDefault<Player>(sql, args);
        }

        public void UpdateIsLoggedIn(int playerId, bool isLoggedIn)
        {
            var sql = @"
                UPDATE players
                SET IsLoggedIn = @isLoggedIn
                WHERE Id = @playerId";
            var args = new { playerId = playerId, isLoggedIn = isLoggedIn ? 1 : 0 };

            Connection.Execute(sql, args, Transaction);
        }
    }
}