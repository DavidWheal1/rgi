﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using RGi.POC_DAL.Repositories.Interfaces;
using RGi.POC_Entities;

namespace RGi.POC_DAL.Repositories
{
    public class RealityCheckRepository : BaseRepository, IRealityCheckRepository
    {
        public RealityCheckRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public void Add(RealityCheck realityCheck)
        {
            var sql = @"
                INSERT INTO reality_check(PlayerId, GameId, Timer)
                VALUES (@playerId, @gameId, @timer);";
            var args = new { playerId = realityCheck.PlayerId, gameId = realityCheck.GameId, realityCheck.Timer };

            Connection.Execute(sql, args, Transaction);
        }

        public IEnumerable<RealityCheck> Get(int playerId)
        {
            var sql = @"
                SELECT PlayerId, GameId, Timer
                FROM reality_check
                WHERE PlayerId = @playerId;";
            var args = new { playerId = playerId };

            return Connection.Query<RealityCheck>(sql, args);
        }

        public void Remove(int playerId, int gameId)
        {
            var sql = @"
                DELETE FROM reality_check
                WHERE PlayerId = @playerId AND GameId = @gameId;";
            var args = new { playerId = playerId, gameId = gameId };

            Connection.Execute(sql, args, Transaction);
        }

        public void Remove(int playerId)
        {
            var sql = @"
                DELETE FROM reality_check
                WHERE PlayerId = @playerId;";
            var args = new { playerId = playerId };

            Connection.Execute(sql, args, Transaction);
        }

        public void UpdateTimer(RealityCheck realityCheck)
        {
            var sql = @"
                UPDATE reality_check
                SET Timer = @timer
                WHERE PlayerId = playerId;";
            var args = new { timer = realityCheck.Timer, playerId = realityCheck.PlayerId, gameId = realityCheck.GameId };

            Connection.Execute(sql, args, Transaction);
        }
    }
}