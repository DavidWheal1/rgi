﻿using Dapper;
using System.Data;

namespace RGi.POC_DAL.Repositories
{
    public abstract class BaseRepository
    {
        protected IDbTransaction Transaction { get; private set; }
        protected IDbConnection Connection { get { return Transaction.Connection; } }

        public BaseRepository(IDbTransaction transaction)
        {
            Transaction = transaction;
        }

        public virtual int ReadCount()
        {
            var sql = @"
                SELECT Count
                FROM counts
                WHERE Id = 1";

            return Connection.ExecuteScalar<int>(sql);
        }

        public virtual void InsertDummy(int count, int transactionId)
        {
            var sql = @"
                INSERT INTO dummies(Count, TransactionId)
                VALUES (@count, @transactionId)";
            var args = new { count = count, transactionId = transactionId };

            Connection.Execute(sql, args, Transaction);
        }

        public virtual void UpdateCount(int count)
        {
            var sql = @"
                UPDATE counts
                SET count = @count
                WHERE Id = 1";
            var args = new { count = count };

            Connection.Execute(sql, args, Transaction);
        }
    }
}