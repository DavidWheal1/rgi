﻿using System.Data;
using MySql.Data.MySqlClient;
using RGi.POC_DAL.Common.Interfaces;
using RGi.POC_DAL.DatabaseTypes.Interfaces;

namespace RGi.POC_DAL.DatabaseTypes
{
    public abstract class BaseDatabaseType : IDatabaseType
    {
        protected readonly string _identifier;

        public BaseDatabaseType(string identifier)
        {
            _identifier = identifier;
        }

        public virtual string GetConnectionString(IConnectionStringProvider connectionStringProvider)
        {
            return connectionStringProvider.GetConnectionString(_identifier);
        }

        public virtual IDbConnection GetConnection(IConnectionStringProvider connectionStringProvider)
        {
            return new MySqlConnection(GetConnectionString(connectionStringProvider));
        }
    }
}