﻿using RGi.POC_DAL.Common.Interfaces;
using System.Data;

namespace RGi.POC_DAL.DatabaseTypes.Interfaces
{
    public interface IDatabaseType
    {
        IDbConnection GetConnection(IConnectionStringProvider connectionStringProvider);
    }
}