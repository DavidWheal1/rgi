﻿using Microsoft.Extensions.Configuration;
using RGi.POC_DAL.Common.Interfaces;
using System;
using System.Configuration;
using System.IO;

namespace RGi.POC_DAL.Common
{
    public class LocalConnectionStringProvider : IConnectionStringProvider
    {
        private readonly IConfigurationRoot _configuration;

        public LocalConnectionStringProvider()
        {
            var configurationBuilder = new ConfigurationBuilder()
                                        .SetBasePath(Directory.GetCurrentDirectory())
                                        .AddJsonFile("appSettings.json", false, true);

            _configuration = configurationBuilder.Build();
        }

        public string GetConnectionString(string databaseName = "poccontrol")
        {
            if (_configuration[databaseName] == null)
            {
                throw new ArgumentNullException("databaseName", "Database configuration name is invalid");

            }

            var connectionString = _configuration[databaseName];

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentNullException("connectionString", "Connection string is empty");
            }

            return connectionString;
        }
    }
}