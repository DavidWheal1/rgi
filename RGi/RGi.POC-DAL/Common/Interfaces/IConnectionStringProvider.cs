﻿namespace RGi.POC_DAL.Common.Interfaces
{
    public interface IConnectionStringProvider
    {
        string GetConnectionString(string databaseName = "poccontrol");
    }
}