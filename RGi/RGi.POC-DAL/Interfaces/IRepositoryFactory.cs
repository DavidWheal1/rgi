﻿namespace RGi.POC_DAL.Interfaces
{
    public interface IRepositoryFactory
    {
        T GetRepository<T>() where T : class;
    }
}