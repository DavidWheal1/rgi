﻿using RGi.POC_DAL.Repositories.Interfaces;
using System;

namespace RGi.POC_DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IGameRepository GameRepository { get; }
        IGameSessionRepository GameSessionRepository { get; }
        IPlayerRepository PlayerRepository { get; }
        IRealityCheckRepository RealityCheckRepository { get; }
        ITransactionRepository TransactionRepository { get; }
        IWalletRepository WalletRepository { get; }
        T GetRepository<T>() where T : class;
        void Commit();
    }
}
