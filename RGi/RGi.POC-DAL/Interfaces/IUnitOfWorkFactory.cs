﻿using RGi.POC_DAL.DatabaseTypes.Interfaces;

namespace RGi.POC_DAL.Interfaces
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork CreateConnection<T>() where T : IDatabaseType, new();
    }
}
