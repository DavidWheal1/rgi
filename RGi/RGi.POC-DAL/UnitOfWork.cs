﻿using RGi.POC_DAL.Interfaces;
using RGi.POC_DAL.Repositories;
using RGi.POC_DAL.Repositories.Interfaces;
using System;
using System.Data;

namespace RGi.POC_DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IRepositoryFactory _repositoryFactory;
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private bool _disposed;
        private IGameRepository _gameRepository;
        private IGameSessionRepository _gameSessionRepository;
        private IPlayerRepository _playerRepository;
        private IRealityCheckRepository _realityCheckRepository;
        private ITransactionRepository _transactionRepository;
        private IWalletRepository _walletRepository;

        public IGameRepository GameRepository
        {
            get { return _gameRepository ?? (_gameRepository = new GameRepository(_transaction)); }
        }

        public IGameSessionRepository GameSessionRepository
        {
            get { return _gameSessionRepository ?? (_gameSessionRepository = new GameSessionRepository(_transaction)); }
        }

        public IPlayerRepository PlayerRepository
        {
            get { return _playerRepository ?? (_playerRepository = new PlayerRepository(_transaction)); }
        }

        public IRealityCheckRepository RealityCheckRepository
        {
            get { return _realityCheckRepository ?? (_realityCheckRepository = new RealityCheckRepository(_transaction)); }
        }

        public ITransactionRepository TransactionRepository
        {
            get { return _transactionRepository ?? (_transactionRepository = new TransactionRepository(_transaction)); }
        }

        public IWalletRepository WalletRepository
        {
            get { return _walletRepository ?? (_walletRepository = new WalletRepository(_transaction)); }
        }

        public UnitOfWork(IDbConnection connection, IRepositoryFactory repositoryFactory)
        {
            _repositoryFactory = repositoryFactory;
            _connection = connection;
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public T GetRepository<T>() where T : class
        {
            return _repositoryFactory.GetRepository<T>();
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                ResetRepositories();
            }
        }

        private void ResetRepositories()
        {
            _gameRepository = null;
            _gameSessionRepository = null;
            _playerRepository = null;
            _realityCheckRepository = null;
            _transactionRepository = null;
            _walletRepository = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }

                    if (_connection != null)
                    {
                        if (_connection.State == ConnectionState.Open)
                        {
                            _connection.Close();
                        }

                        _connection.Dispose();
                        _connection = null;
                    }
                }

                _disposed = true;
            }
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
    }
}