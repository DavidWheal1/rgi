﻿using RGi.POC_DAL.Common.Interfaces;
using RGi.POC_DAL.DatabaseTypes.Interfaces;
using RGi.POC_DAL.Interfaces;

namespace RGi.POC_DAL
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IConnectionStringProvider _connectionStringProvider;
        private readonly IRepositoryFactory _repositoryFactory;

        public UnitOfWorkFactory(IConnectionStringProvider connectionStringProvider, IRepositoryFactory repositoryFactory)
        {
            _connectionStringProvider = connectionStringProvider;
            _repositoryFactory = repositoryFactory;
        }

        public IUnitOfWork CreateConnection<T>() where T : IDatabaseType, new()
        {
            T databaseType = new T();
            var connection = databaseType.GetConnection(_connectionStringProvider);

            return new UnitOfWork(connection, _repositoryFactory);
        }
    }
}
