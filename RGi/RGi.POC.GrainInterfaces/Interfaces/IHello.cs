﻿using System.Threading.Tasks;
using Orleans.CodeGeneration;

namespace RGi.POC.GrainInterfaces.Interfaces
{
    /// <summary>
    /// Orleans grain communication interface IHello
    /// </summary>
    [Version(1)]
    public interface IHello : Orleans.IGrainWithIntegerKey
    {
        Task<string> SayHello(string greeting);
        Task Reset();
    }
}
