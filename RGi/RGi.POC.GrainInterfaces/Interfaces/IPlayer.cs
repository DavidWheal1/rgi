﻿using Orleans;
using Orleans.CodeGeneration;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RGi.POC_Entities;

namespace RGi.POC.GrainInterfaces.Interfaces
{
    [Version(1)]
    public interface IPlayer : IGrainWithIntegerKey
    {
        Task<bool> IsLoggedIn();
        Task<List<Game>> GetGamesList();
        Task<bool> SetLoggedin();
        Task Logout();
        Task<int> Win(int gameSessionId,int gameId, decimal amount);
        Task<int> Lose(int gameSessionId, int gameId, decimal amount);
        Task<decimal> Balance(int playerId);
        Task<int> StartGame(int gameId, bool isExternal, Guid token);
        Task StopGame(int gameId, int gameSessionId);
    }
}