﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Orleans;
using Orleans.CodeGeneration;

namespace RGi.POC.GrainInterfaces.Interfaces
{
    [Version(1)]
    public interface IGame : IGrainWithIntegerKey
    {
    }
}
