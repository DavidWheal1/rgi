﻿using Orleans;
using Orleans.CodeGeneration;
using System;
using System.Threading.Tasks;

namespace RGi.POC.GrainInterfaces.Interfaces
{
    [Version(1)]
    public interface IGameSession : IGrainWithIntegerCompoundKey
    {
        Task<int> Start(Guid token);
        Task Stop(int gameSessionId);
    }
}