﻿using Orleans;
using Orleans.CodeGeneration;
using RGi.POC_Models.Requests;
using System.Threading.Tasks;

namespace RGi.POC.GrainInterfaces.Interfaces
{
    [Version(1)]
    public interface ITransactionManager : IGrainWithGuidKey
    {
        Task<int> Credit(TransactionRequest transactionRequest);
        Task<int> Debit(TransactionRequest transactionRequest);

    }
}