﻿using Orleans;
using Orleans.CodeGeneration;
using System.Threading.Tasks;

namespace RGi.POC.GrainInterfaces.Interfaces
{
    [Version(1)]
    public interface IExternalGame : IGrainWithGuidKey
    {
        Task Launch();
    }
}