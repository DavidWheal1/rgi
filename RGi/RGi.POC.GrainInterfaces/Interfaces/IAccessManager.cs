﻿using Orleans;
using Orleans.CodeGeneration;
using RGi.POC_Models.Requests;
using System.Threading.Tasks;
using RGi.POC_Entities;

namespace RGi.POC.GrainInterfaces.Interfaces
{
    [Version(1)]
    public interface IAccessManager : IGrainWithGuidKey
    {
        Task<IPlayer> Login(LoginRequest loginRequest);
    }
}