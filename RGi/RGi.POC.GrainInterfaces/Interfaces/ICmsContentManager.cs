﻿using Orleans;
using Orleans.CodeGeneration;
using System.Collections.Generic;
using System.Threading.Tasks;
using RGi.POC_Entities;

namespace RGi.POC.GrainInterfaces.Interfaces
{
    [Version(1)]
    public interface ICmsContentManager : IGrainWithGuidKey
    {
        Task<List<Game>> List();
        Task<Game> Get(int gameId);
    }
}