﻿using Orleans;
using Orleans.CodeGeneration;
using RGi.POC_Models.Enums;
using RGi.POC_Models.Requests;
using System.Threading.Tasks;

namespace RGi.POC.GrainInterfaces.Interfaces
{
    [Version(1)]
    public interface IRealityCheck : IGrainWithIntegerKey
    {
        Task Setup();
        Task StartGame(int gameId);
        Task StopGame(int gameId);
        Task<RealityCheckCheckEnum> Check();
        Task Result(RealityCheckResultRequest realityCheckResultRequest);
    }
}