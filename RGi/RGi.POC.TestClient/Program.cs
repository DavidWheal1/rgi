﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Runtime;
using RGi.POC.GrainInterfaces.Interfaces;
using RGi.POC_Models.Requests;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Polly;

namespace RGi.POC.TestClient
{
    /// <summary>
    /// Orleans test silo client
    /// </summary>
    public class Program
    {

        private static ClientConfig clientConfig = new ClientConfig();

        static int Main(string[] args)
        {

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var _config = builder.Build();

            clientConfig.ClusterId = _config["ClusterId"];
            clientConfig.ServiceId = _config["ServiceId"];
            clientConfig.ClusterProviderConnectionString = _config["ClusterProviderConnectionString"];
            clientConfig.ClusterProviderInvariant = _config["ClusterProviderInvariant"];

            return RunMainAsync().Result;
        }

        private static async Task<int> RunMainAsync()
        {


            try
            {


                await DoClientWork();
                Console.ReadKey();

                return 0;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
                return 1;
            }
        }



        private static async Task<IClusterClient> StartAdoClusterClientWithRetries(int initializeAttemptsBeforeFailing = 5)
        {
            int attempt = 0;
            IClusterClient client;
            while (true)
            {
                try
                {
#if DEBUG
                    client = new ClientBuilder()
                        .UseLocalhostClustering()
                        .Configure<ClusterOptions>(options =>
                        {
                            options.ClusterId = clientConfig.ClusterId;
                            options.ServiceId = clientConfig.ServiceId;
                        })
                        .ConfigureLogging(logging => logging.AddConsole())

                        .Build();
#else
                    client = new ClientBuilder()
                        .UseAdoNetClustering(options =>
                        {
                            options.Invariant = clientConfig.ClusterProviderInvariant;
                            options.ConnectionString = clientConfig.ClusterProviderConnectionString;
                        })
                        .Configure<ClusterOptions>(options =>
                        {
                            options.ClusterId = clientConfig.ClusterId;
                            options.ServiceId = clientConfig.ServiceId;
                        })
                        .ConfigureLogging(logging => logging.AddConsole())

                        .Build();
#endif


                    await client.Connect();
                    Console.WriteLine("Client successfully connect to silo host");
                    break;
                }
                catch (SiloUnavailableException)
                {
                    attempt++;
                    Console.WriteLine($"Attempt {attempt} of {initializeAttemptsBeforeFailing} failed to initialize the (Ado) Orleans client.");
                    if (attempt > initializeAttemptsBeforeFailing)
                    {
                        throw;
                    }
                    await Task.Delay(TimeSpan.FromSeconds(4));
                }
            }

            return client;
        }


        private static async Task DoClientWork()
        {
            try
            {
                IClusterClient client = null;
                client = await OrleansHelper.CreateClusterClientAsync();

                Policy retryPolicy = Policy
                        .Handle<OrleansMessageRejectionException>()
                        .RetryAsync(1, async (e, i) =>
                        {
                            client = await OrleansHelper.CreateClusterClientAsync();
                        })
                    ;
                await retryPolicy.ExecuteAsync(async () =>
                {
                    var cms = client.GetGrain<ICmsContentManager>(Guid.Empty);

                    var g = client.GetGrain<IAccessManager>(Guid.Empty);
                    var playerGrain = await g.Login(new LoginRequest() { Username = "marco0", Password = "password0" });
                    var loggedIn = await playerGrain.IsLoggedIn();

                    var gameSessionGrain = client.GetGrain<IGameSession>(playerGrain.GetPrimaryKeyLong(), "124", null);
                    var token = Guid.NewGuid();

                    var session = await gameSessionGrain.Start(token);
                    Thread.Sleep(5000);
                    await playerGrain.Win(session, 1, 23);
                    await gameSessionGrain.Stop(session);
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


            //string command = string.Empty;
            //var grains = new List<IHello>();
            //for (int i = 0; i < 1000; i++)
            //{
            //    grains.Add(client.GetGrain<IHello>(i));
            //}

            //var loops = 5;
            //double sum = 0;
            //// example of calling grains from the initialized client
            ////var friend = client.GetGrain<IHello>(0);
            //for (int i = 0; i < loops; i++)
            //{

            //    Stopwatch sw = new Stopwatch();
            //    sw.Start();
            //    foreach (var g in grains)
            //    {
            //        await g.Reset();


            //        var response = await g.SayHello($"Test the hello grain at utc {DateTime.UtcNow}");
            //        //Console.WriteLine("{0}\n", response);
            //    }

            //    sw.Stop();
            //    sum += sw.Elapsed.TotalSeconds;
            //}

            //Console.WriteLine($"Took {sum/loops} seconds");
            var k = Console.ReadLine();
        }
    }
}
