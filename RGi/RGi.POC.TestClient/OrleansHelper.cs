﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using RGi.POC.OrleansDal;
using RGi.POC.OrleansDal.Common;
using RGi.POC.OrleansDal.Common.Interfaces;
using RGi.POC.OrleansDal.Interfaces;

namespace RGi.POC.TestClient
{
    public static class OrleansHelper
    {

        public static async Task<IClusterClient> CreateClusterClientAsync()
        {
            var clientBuilder = BuildClusterClient();
            var client = clientBuilder.Build();

            await client.Connect(async ex =>
            {
                Console.WriteLine(ex);
                Console.WriteLine("Retrying...");
                await Task.Delay(3000);
                return true;
            });
            return client;
        }

        public static IClientBuilder BuildClusterClient()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var config = configBuilder.Build();
            var clientBuilder = new ClientBuilder()
#if DEBUG
                                .UseLocalhostClustering()
#else
                .UseAdoNetClustering(options =>
                {
                    options.Invariant = config["ClusterProviderInvariant"];
                    options.ConnectionString = config["ClusterProviderConnectionString"];
                })
#endif
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = config["ClusterId"];
                    options.ServiceId = config["ServiceId"];
                })
                .ConfigureLogging(logging => logging.AddConsole())
                .ConfigureServices(svc =>
                {
                    svc.AddSingleton<IConnectionStringProvider, LocalConnectionStringProvider>();
                    svc.AddTransient<IConnectionStringProvider>(s => new LocalConnectionStringProvider(config["ClusterProviderConnectionString"]));
                    svc.AddSingleton<IRepositoryFactory, RepositoryFactory>();
                });

            return clientBuilder;
        }
    }
}
