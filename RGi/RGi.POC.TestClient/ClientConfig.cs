﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace RGi.POC.TestClient
{
    public class ClientConfig
    {
        public string ClusterId { get; set; }
        public string ServiceId { get; set; }
        public string ClusterProviderConnectionString { get; set; }
        public string ClusterProviderInvariant { get; set; }
    }
}
