﻿using log4net.Config;
using RGi.POC.SiloHostService.Orleans;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;

namespace RGi.POC.SiloHostService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            XmlConfigurator.Configure();

#if DEBUG
            MainAsync().Wait();
#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new SiloHostService()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }

        static async Task MainAsync()
        {
            // your async code here
            PocSilo.StartDevClusterSilo();
            while (1 == 1)
            {
                Thread.Sleep(1000);
            }
        }

    }
}
