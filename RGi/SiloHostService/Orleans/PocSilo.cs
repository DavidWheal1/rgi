﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Logging;
using Orleans.Versions.Compatibility;
using Orleans.Versions.Selector;
using RGi.POC.Grains.Grains;
using RGi.POC.OrleansDal;
using RGi.POC.OrleansDal.Common;
using RGi.POC.OrleansDal.Common.Interfaces;
using RGi.POC.OrleansDal.DatabaseTypes;
using RGi.POC.OrleansDal.Interfaces;
using RGi.POC.SiloHostService.Configuration;

namespace RGi.POC.SiloHostService.Orleans
{
    public static class PocSilo
    {
        private static SiloConfiguration _config = new SiloConfiguration();
        private static ISiloHost _mySiloHost;

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public static void StartAdoNetClusterSilo()
        {
            try
            {
                // define the cluster configuration
                var builder = new SiloHostBuilder()
                        .Configure<ClusterOptions>(options =>
                        {
                            options.ClusterId = _config.ClusterId;
                            options.ServiceId = _config.ServiceId;
                        })
                        .UseDashboard(options =>
                        {
                            options.Port = 8080;
                            options.Username = "admin";
                            options.Password = "admin";
                            options.Host = "*";
                            options.HostSelf = true;
                        })

                        .UseAdoNetClustering(options =>
                        {
                            options.Invariant = _config.ClusterProviderInvariant;
                            options.ConnectionString = _config.ClusterProviderConnectionString;
                        }).AddAdoNetGrainStorage(_config.GrainStorageName, options =>
                        {
                            options.Invariant = _config.ClusterProviderInvariant;
                            options.ConnectionString = _config.ClusterProviderConnectionString;
                        })
                        .Configure<EndpointOptions>(options =>
                        {
                            // Port to use for Silo-to-Silo
                            options.SiloPort = 11111;
                            // Port to use for the gateway
                            options.GatewayPort = 30000;
                            // IP Address to advertise in the cluster
                            options.AdvertisedIPAddress = _config.AdvertisedIpAddress; // Public IP of this box
                            // The socket used for silo-to-silo will bind to this endpoint
                            options.GatewayListeningEndpoint = new IPEndPoint(IPAddress.Any, 30000);
                            // The socket used by the gateway will bind to this endpoint
                            options.SiloListeningEndpoint = new IPEndPoint(IPAddress.Any, 11111);

                        })

                        .ConfigureApplicationParts(parts =>
                        {
                            parts.AddApplicationPart(typeof(HelloGrain).Assembly).WithReferences();
                            parts.AddApplicationPart(typeof(RealityCheckGrain).Assembly).WithReferences();
                            parts.AddApplicationPart(typeof(TransactionManagerGrain).Assembly).WithReferences();
                            parts.AddApplicationPart(typeof(AccessManagerGrain).Assembly).WithReferences();
                            parts.AddApplicationPart(typeof(CmsContentManagerGrain).Assembly).WithReferences();
                            parts.AddApplicationPart(typeof(PlayerGrain).Assembly).WithReferences();
                            parts.AddApplicationPart(typeof(GameSessionGrain).Assembly).WithReferences();
                            parts.AddApplicationPart(typeof(GameGrain).Assembly).WithReferences();
                        })
                        .Configure<GrainVersioningOptions>(options =>
                        {
                            options.DefaultCompatibilityStrategy = nameof(BackwardCompatible);
                            options.DefaultVersionSelectorStrategy = nameof(MinimumVersion);
                        })
                        .ConfigureServices(svc =>
                        {
                            //svc.AddSingleton<IConnectionStringProvider, LocalConnectionStringProvider>();
                            svc.AddTransient<IConnectionStringProvider>(s => new LocalConnectionStringProvider(ConfigurationManager.AppSettings["pocorleans"]));
                            svc.AddSingleton<IRepositoryFactory, RepositoryFactory>();
                        })
                    ;

                var logConfigPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);
                logConfigPath = Path.Combine(logConfigPath, "log4net.config");
                builder.ConfigureLogging(options =>
                {
                    //options.AddFilter("Orleans", LogLevel.Debug);
                    //options.AddFile(@"c:\logs\orleans.log");
                    options.AddLog4Net(logConfigPath);
                });

                //builder.UseAdoNetReminderService(options =>
                //{
                //    options.Invariant = _config.ClusterProviderInvariant;
                //    options.ConnectionString = _config.ClusterProviderConnectionString;
                //});

                _mySiloHost = builder.Build();
                _mySiloHost.StartAsync();
                Log.Debug($"Poc Server Silo Started");

            }
            catch (Exception e)
            {
                Log.Error("Failed while trying to start the Poc Silo Host (MySql)", e);
            }

        }

        public static void StartDevClusterSilo()
        {
            try
            {
                // define the cluster configuration
                var builder = new SiloHostBuilder()
                        .Configure<ClusterOptions>(options =>
                        {
                            options.ClusterId = _config.ClusterId;
                            options.ServiceId = _config.ServiceId;
                        })
                        .UseDashboard(options =>
                        {
                            options.Port = 8080;
                            options.Username = "admin";
                            options.Password = "admin";
                            options.Host = "*";
                            options.HostSelf = true;
                        })

                        .UseLocalhostClustering()
                        .AddAdoNetGrainStorage(_config.GrainStorageName, options =>
                        {
                            options.Invariant = _config.ClusterProviderInvariant;
                            options.ConnectionString = _config.ClusterProviderConnectionString;
                        })
                        .Configure<EndpointOptions>(options => options.AdvertisedIPAddress = IPAddress.Loopback)

                        .ConfigureApplicationParts(parts =>
                            {
                                parts.AddApplicationPart(typeof(HelloGrain).Assembly).WithReferences();
                                parts.AddApplicationPart(typeof(AccessManagerGrain).Assembly).WithReferences();
                                parts.AddApplicationPart(typeof(CmsContentManagerGrain).Assembly).WithReferences();
                                parts.AddApplicationPart(typeof(PlayerGrain).Assembly).WithReferences();
                                parts.AddApplicationPart(typeof(GameSessionGrain).Assembly).WithReferences();
                                parts.AddApplicationPart(typeof(GameGrain).Assembly).WithReferences();
                                parts.AddApplicationPart(typeof(RealityCheckGrain).Assembly).WithReferences();
                                parts.AddApplicationPart(typeof(TransactionManagerGrain).Assembly).WithReferences();

                            })

                        .Configure<GrainVersioningOptions>(options =>
                        {
                            options.DefaultCompatibilityStrategy = nameof(BackwardCompatible);
                            options.DefaultVersionSelectorStrategy = nameof(MinimumVersion);
                        })
                        .ConfigureServices(svc =>
                        {
                            svc.AddSingleton<IConnectionStringProvider, LocalConnectionStringProvider>();
                            svc.AddTransient<IConnectionStringProvider>(s => new LocalConnectionStringProvider(ConfigurationManager.AppSettings["pocorleans"]));
                            svc.AddSingleton<IRepositoryFactory, RepositoryFactory>();
                        })
                    ;



                var logConfigPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);
                logConfigPath = Path.Combine(logConfigPath, "log4net.config");
                builder.ConfigureLogging(options =>
                {
                    //options.AddFilter("Orleans", LogLevel.Debug);
                    //options.AddFile(@"c:\logs\orleans.log");
                    options.AddLog4Net(logConfigPath);
                });

                //builder.UseAdoNetReminderService(options =>
                //{
                //    options.Invariant = _config.ClusterProviderInvariant;
                //    options.ConnectionString = _config.ClusterProviderConnectionString;
                //});

                _mySiloHost = builder.Build();
                _mySiloHost.StartAsync();
                Log.Debug($"Poc Server Silo Started");

            }
            catch (Exception e)
            {
                Log.Error("Failed while trying to start the Poc Silo Host (dev)", e);
            }

        }

        public static async Task StopSilo()
        {
            try
            {
                await _mySiloHost.StopAsync();

            }
            catch (Exception e)
            {
                Log.Error("Failed while trying to stop the Poc Silo Host", e);
            }
        }
    }
}
