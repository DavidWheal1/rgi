﻿using System.ServiceProcess;
using RGi.POC.SiloHostService.Orleans;

namespace RGi.POC.SiloHostService
{
    public partial class SiloHostService : ServiceBase
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SiloHostService()
        {
            Log.Debug($"SiloHostService:Init");
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Log.Debug($"SiloHostService:OnStart");
            PocSilo.StartAdoNetClusterSilo();
            Log.Debug($"SiloHostService:OnStart Done");
        }

        protected override async void OnStop()
        {
            Log.Debug($"SiloHostService:OnStop");
            await PocSilo.StopSilo();
            Log.Debug($"SiloHostService:OnStop Done");
        }


    }
}
