﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Runtime;
using RGi.POC.OrleansDal;
using RGi.POC.OrleansDal.Common;
using RGi.POC.OrleansDal.Common.Interfaces;
using RGi.POC.OrleansDal.Interfaces;


namespace RGi.POCGateway.PostSharp
{
    public static class ClusterClientFactory
    {
        private static ILogger _logger;

        static SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

        public enum ClusterStatus
        {
            Ok,
            Retrying,
            Down
        }

        private static ClusterStatus _status = ClusterStatus.Retrying;
        private static IClusterClient ClusterClient { get; set; }

        public static void SetLogger(ILogger logger)
        {
            _logger = logger;
        }
        public static async Task<IClusterClient> Get()
        {
            switch (_status)
            {
                case ClusterStatus.Ok:
                    _logger.Debug($"Status ok");
                    return ClusterClient;
                case ClusterStatus.Retrying:
                    // Cannot lock async
                    await _semaphore.WaitAsync();
                    try
                    {
                        if (_status != ClusterStatus.Ok)
                        {
                            _logger.Debug($"Status retrying");
                            ClusterClient = await CreateClusterClientAsync();
                            _status = ClusterStatus.Ok;
                        }

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        _semaphore.Release();
                    }
                    return ClusterClient;
                default:
                    _logger.Debug($"Status not good at all");
                    return null;
            }
        }

        public static void SetRetryStatus(Exception e)
        {
            if (e is OrleansMessageRejectionException)
            {
                _logger.Debug($"Status being set to retrying");
                _status = ClusterStatus.Retrying;
                return;
            }

        }

        private static async Task<IClusterClient> CreateClusterClientAsync()
        {
            _logger.Debug($"CreateClusterClientAsync");
            var clientBuilder = BuildClusterClient();
            var client = clientBuilder.Build();

            await client.Connect(async ex =>
            {
                _logger.Error(-90, "Failed a retry connection", ex);
                await Task.Delay(3000);
                return true;
            });
            _logger.Debug($"Cluster connected ok");
            return client;
        }

        private static IClientBuilder BuildClusterClient()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var config = configBuilder.Build();
            var clientBuilder = new ClientBuilder()
#if DEBUG
                                .UseLocalhostClustering()
#else
                .UseAdoNetClustering(options =>
                {
                    options.Invariant = config["ClusterProviderInvariant"];
                    options.ConnectionString = config["ClusterProviderConnectionString"];
                })
#endif
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = config["ClusterId"];
                    options.ServiceId = config["ServiceId"];
                })
                .ConfigureLogging(logging => logging.AddConsole())
                .ConfigureServices(svc =>
                {
                    svc.AddSingleton<IConnectionStringProvider, LocalConnectionStringProvider>();
                    svc.AddTransient<IConnectionStringProvider>(s => new LocalConnectionStringProvider(config["ClusterProviderConnectionString"]));
                    svc.AddSingleton<IRepositoryFactory, RepositoryFactory>();
                });

            return clientBuilder;
        }
    }
}
