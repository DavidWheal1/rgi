﻿using Microsoft.AspNetCore.Mvc;
using Orleans;
using RGi.POC.GrainInterfaces.Interfaces;
using RGi.POC_Gateway.Interfaces;
using RGi.POC_Models.Enums;
using RGi.POC_Models.Requests;
using RGi.POC_Models.ViewModels;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Internal;
using Microsoft.Extensions.Logging;
using Orleans.Runtime;
using RGi.POCGateway.PostSharp;

namespace RGi.POC_Gateway.Controllers.Orleans
{
    [Produces("application/json")]
    [Route("api/orleans_player")]
    public class PlayerController : BaseController, IOrleansPlayerController
    {
        public PlayerController(ILoggerFactory factory)
            : base(factory)
        {
        }

        [HttpPost("login")]
        public async Task<Player> Login([FromBody]LoginRequest loginRequest)
        {
            return await RetryPolicy.ExecuteAsync(async () =>
            {
                try
                {
                    _logger.Debug(0, $"Login for {loginRequest.Username}");
                    var accessManager = (await ClusterClientFactory.Get()).GetGrain<IAccessManager>(Guid.Empty);
                    var player = await accessManager.Login(loginRequest);

                    return new Player
                    {
                        Id = (int)player.GetPrimaryKeyLong()
                    };
                }
                catch (Exception ex)
                {
                    _logger.Error(-98, $"Login failed", ex);
                    throw;

                    //return null;
                }
            });
        }

        [HttpDelete("logout/{playerId:int}")]
        public async Task Logout(int playerId)
        {
            await RetryPolicy.ExecuteAsync(async () =>
            {
                try
                {
                    var player = (await ClusterClientFactory.Get()).GetGrain<IPlayer>(playerId);

                    await player.Logout();
                }
                catch (Exception ex)
                {
                    _logger.Error(-97, "Logout failed", ex);
                    throw;
                }
            });
        }

        [HttpGet("realitycheckcheck/{playerId:int}")]
        public async Task<RealityCheckCheckEnum> RealityCheckCheck(int playerId)
        {
            return await RetryPolicy.ExecuteAsync(async () =>
            {
                try
                {
                    var realityCheck = (await ClusterClientFactory.Get()).GetGrain<IRealityCheck>(playerId);

                    return await realityCheck.Check();
                }
                catch (Exception ex)
                {
                    _logger.Error(-96, "RealityCheckCheck failed", ex);
                    throw;
                    //return RealityCheckCheckEnum.Unknown;
                }
            });
        }

        [HttpPut("realitycheckresult")]
        public async Task RealityCheckResult(RealityCheckResultRequest realityCheckResultRequest)
        {

            await RetryPolicy.ExecuteAsync(async () =>
            {
                try
                {
                    var realityCheck =
                        (await ClusterClientFactory.Get()).GetGrain<IRealityCheck>(realityCheckResultRequest.PlayerId);

                    await realityCheck.Result(realityCheckResultRequest);
                }
                catch (Exception ex)
                {
                    _logger.Error(-95, "RealityCheckResult failed", ex);
                    throw;

                }
            });
        }

        [HttpPost("debit")]
        public async Task<int> Stake([FromBody]TransactionRequest transactionRequest)
        {

            return await RetryPolicy.ExecuteAsync(async () =>
            {
                try
                {
                    var player = (await ClusterClientFactory.Get()).GetGrain<IPlayer>(transactionRequest.PlayerId);

                    return await player.Lose(transactionRequest.GameSessionId, transactionRequest.GameId,
                        transactionRequest.Amount);
                }
                catch (Exception ex)
                {
                    _logger.Error(-94, "Stake failed", ex);
                    throw;
                    //return -1;
                }
            });

        }

        [HttpPost("play")]
        public async Task<int> StartPlay([FromBody]GameSessionStartRequest gameSessionRequest)
        {
            return await RetryPolicy.ExecuteAsync(async () =>
            {
                try
                {
                    var gameSession = (await ClusterClientFactory.Get()).GetGrain<IGameSession>(
                        gameSessionRequest.PlayerId,
                        gameSessionRequest.GameId.ToString(), null);

                    return await gameSession.Start(Guid.NewGuid());
                }
                catch (Exception ex)
                {
                    _logger.Error(-93, "StartPlay failed", ex);
                    throw;
                    //return -1;
                }
            });
        }

        [HttpPut("stop")]
        public async Task StopPlay([FromBody]GameSessionStopRequest gameSessionRequest)
        {
            await RetryPolicy.ExecuteAsync(async () =>
            {
                try
                {
                    var gameSession = (await ClusterClientFactory.Get()).GetGrain<IGameSession>(
                        gameSessionRequest.PlayerId,
                        gameSessionRequest.GameId.ToString(), null);

                    await gameSession.Stop(gameSessionRequest.GameSessionId);
                }
                catch (Exception ex)
                {
                    _logger.Error(-92, "StopPlay failed", ex);
                    throw;
                }
            });

        }

        [HttpPost("credit")]
        public async Task<int> Win([FromBody]TransactionRequest transactionRequest)
        {
            return await RetryPolicy.ExecuteAsync(async () =>
            {
                try
                {
                    var player = (await ClusterClientFactory.Get()).GetGrain<IPlayer>(transactionRequest.PlayerId);

                    return await player.Win(transactionRequest.GameSessionId, transactionRequest.GameId,
                        transactionRequest.Amount);
                }
                catch (Exception ex)
                {
                    _logger.Error(-91, "Win failed", ex);
                    throw;
                    //return -1;
                }
            });
        }
    }
}