﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;
using Polly;
using RGi.POCGateway.PostSharp;
using Serilog;
using ILogger = Serilog.ILogger;

namespace RGi.POC_Gateway.Controllers.Orleans
{
    public class BaseController : Controller
    {
        protected ILoggerFactory _factory;
        protected Microsoft.Extensions.Logging.ILogger _logger;
        protected Policy RetryPolicy;

        public BaseController(ILoggerFactory factory)
        {
            _factory = factory;
            _logger = _factory.CreateLogger("StuffyWuffy");
            ClusterClientFactory.SetLogger(_logger);
            RetryPolicy = Policy
                .Handle<Exception>()
                .RetryAsync(1, (e, i) =>
                {
                    _logger.Error(-99, $"Retry policy in action", e);
                    ClusterClientFactory.SetRetryStatus(e);
                })
                ;

        }
    }
}