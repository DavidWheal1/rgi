﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;
using RGi.POC.GrainInterfaces.Interfaces;
using RGi.POCGateway.PostSharp;
using RGi.POC_Gateway.Interfaces;
using RGi.POC_Models.ViewModels;

namespace RGi.POC_Gateway.Controllers.Orleans
{
    [Produces("application/json")]
    [Route("api/orleans_games")]
    public class GamesController : BaseController, IOrleansGameController
    {
        public GamesController(ILoggerFactory factory)
            : base(factory)
        {
        }


        [HttpGet("{gameId:int}")]
        public async Task<string> GetGame(int gameId)
        {
            
            return await RetryPolicy.ExecuteAsync(async () =>
            {
                var cmsContentManager = (await ClusterClientFactory.Get()).GetGrain<ICmsContentManager>(Guid.Empty);
                return (await cmsContentManager.Get(gameId)).Name;
            });

        }

        [HttpGet]
        public async Task<List<Game>> GetGamesList()
        {
            return await RetryPolicy.ExecuteAsync(async () =>
            {
                var cmsContentManager = (await ClusterClientFactory.Get()).GetGrain<ICmsContentManager>(Guid.Empty);
                var games = await cmsContentManager.List();

                return games.Select(g => new Game
                {
                    Id = g.Id,
                    Name = g.Name
                }).ToList();
            });
        }
    }
}