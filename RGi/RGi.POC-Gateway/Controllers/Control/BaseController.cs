﻿using Microsoft.AspNetCore.Mvc;
using RestSharp;
using Serilog;

namespace RGi.POC_Gateway.Controllers.Control
{
    public class BaseController : Controller
    {
        protected IRestClient RestClient;
        protected IRestRequest RestRequest;
        protected ILogger Logger;

        public BaseController()
        {

            RestClient = new RestClient(Gateway.GetBusinessLogicLayerUrl());
            RestRequest = new RestRequest
            {
                RequestFormat = DataFormat.Json                
            };
            Logger = new LoggerConfiguration().WriteTo.RollingFile(@"C:\log\RGi.POC-Gateway-Control-{Date}.txt").CreateLogger();
        }
    }
}