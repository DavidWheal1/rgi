﻿using Microsoft.AspNetCore.Mvc;
using RGi.POC_Gateway.Interfaces;
using RGi.POC_Models.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;

namespace RGi.POC_Gateway.Controllers.Control
{
    [Produces("application/json")]
    [Route("api/games")]
    public class GamesController : BaseController, IGameController
    {
        
        [HttpGet("{test:int}")]
        public string GetRelease()
        {
            try
            {
               
                return "1.0";
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;

                //return null;
            }
        }
        
        
        [HttpGet]
        public List<Game> GetGamesList()
        {
            try
            {
                RestRequest.Resource = "api/bl_games";

                var restResponse = RestClient.Execute<List<Game>>(RestRequest);

                if (restResponse.ErrorException != null)
                {
                    throw restResponse.ErrorException;
                }

                return restResponse.Data;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;

                //return null;
            }
        }




        [HttpGet("{gameId:int}")]
        public string GetGame(int gameId)
        {
            try
            {
                RestRequest.Resource = $"api/bl_games/{gameId}";

                var restResponse = RestClient.Execute<Game>(RestRequest);

                if (restResponse.ErrorException != null)
                {
                    throw restResponse.ErrorException;
                }

                return restResponse.Data.Name;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;

                //return null;
            }
        }
    }
}