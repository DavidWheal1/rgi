﻿using Microsoft.AspNetCore.Mvc;
using RestSharp;
using RGi.POC_Gateway.Interfaces;
using RGi.POC_Models.Enums;
using RGi.POC_Models.Requests;
using RGi.POC_Models.ViewModels;
using System;
using System.Net;
using Microsoft.Extensions.Logging;

namespace RGi.POC_Gateway.Controllers.Control
{
    [Produces("application/json")]
    [Route("api/player")]
    public class PlayerController : BaseController, IPlayerController
    {
      

        [HttpPost("login")]
        public Player Login([FromBody]LoginRequest loginRequest)
        {
            try
            {
                RestRequest.Resource = "api/bl_player/login";
                RestRequest.Method = Method.POST;
                RestRequest.AddBody(loginRequest);

                var restResponse = RestClient.Execute<Player>(RestRequest);

                if (restResponse.ErrorException != null)
                {
                    throw restResponse.ErrorException;
                }

                return restResponse.Data;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;
            }
        }

        [HttpDelete("logout/{playerId:int}")]
        public void Logout(int playerId)
        {
            try
            {
                RestRequest.Resource = $"api/bl_player/logout/{playerId}";
                RestRequest.Method = Method.DELETE;

                var restResponse = RestClient.Execute(RestRequest);

                if (restResponse.ErrorException != null)
                {
                    throw restResponse.ErrorException;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;

            }
        }

        [HttpPost("play")]
        public int StartPlay([FromBody]GameSessionStartRequest gameSessionRequest)
        {
            try
            {
                RestRequest.Resource = "api/bl_gamesession/start";
                RestRequest.Method = Method.POST;
                RestRequest.AddBody(gameSessionRequest);

                var restResponse = RestClient.Execute<int>(RestRequest);

                if (restResponse.ErrorException != null)
                {
                    throw restResponse.ErrorException;
                }

                return restResponse.Data;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;

                //return -1;
            }
        }

        [HttpPut("stop")]
        public void StopPlay([FromBody]GameSessionStopRequest gameSessionRequest)
        {
            try
            {
                RestRequest.Resource = "api/bl_gamesession/stop";
                RestRequest.Method = Method.PUT;
                RestRequest.AddBody(gameSessionRequest);

                var restResponse = RestClient.Execute(RestRequest);

                if (restResponse.ErrorException != null)
                {
                    throw restResponse.ErrorException;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;

            }
        }

        [HttpGet("realitycheckcheck/{playerId:int}")]
        public RealityCheckCheckEnum RealityCheckCheck(int playerId)
        {
            try
            {
                RestRequest.Resource = $"api/bl_realitycheck/check/{playerId}";

                var restResponse = RestClient.Execute<RealityCheckCheckEnum>(RestRequest);

                if (restResponse.ErrorException != null)
                {
                    throw restResponse.ErrorException;
                }

                return restResponse.Data;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;

                //return RealityCheckCheckEnum.Unknown;
            }
        }

        [HttpPut("realitycheckresult")]
        public void RealityCheckResult([FromBody]RealityCheckResultRequest realityCheckResultRequest)
        {
            try
            {
                RestRequest.Resource = $"api/bl_realitycheck/result";
                RestRequest.Method = Method.PUT;
                RestRequest.AddBody(realityCheckResultRequest);

                var restResponse = RestClient.Execute<RealityCheckCheckEnum>(RestRequest);

                if (restResponse.ErrorException != null)
                {
                    throw restResponse.ErrorException;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;

            }
        }

        [HttpPost("credit")]
        public int Win([FromBody]TransactionRequest transactionRequest)
        {
            try
            {
                RestRequest.Resource = "api/bl_transactions/credit";
                RestRequest.Method = Method.POST;
                RestRequest.AddBody(transactionRequest);

                var restResponse = RestClient.Execute<int>(RestRequest);

                if (restResponse.ErrorException != null)
                {
                    throw restResponse.ErrorException;
                }

                return restResponse.Data;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;

                //return -1;
            }
        }

        [HttpPost("debit")]
        public int Stake([FromBody]TransactionRequest transactionRequest)
        {
            try
            {
                RestRequest.Resource = "api/bl_transactions/debit";
                RestRequest.Method = Method.POST;
                RestRequest.AddBody(transactionRequest);

                var restResponse = RestClient.Execute<int>(RestRequest);

                if (restResponse.ErrorException != null)
                {
                    throw restResponse.ErrorException;
                }

                return restResponse.Data;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;

                //return -1;
            }
        }
    }
}