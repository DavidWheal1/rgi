﻿using RGi.POC_Models.ViewModels;
using System.Collections.Generic;

namespace RGi.POC_Gateway.Interfaces
{
    public interface IGameController
    {
        List<Game> GetGamesList();
        string GetGame(int gameId);
    }
}