﻿using RGi.POC_Models.Enums;
using RGi.POC_Models.Requests;
using RGi.POC_Models.ViewModels;

namespace RGi.POC_Gateway.Interfaces
{
    public interface IPlayerController
    {
        Player Login(LoginRequest loginRequest);
        void Logout(int playerId);
        int StartPlay(GameSessionStartRequest gameSessionRequest);
        void StopPlay(GameSessionStopRequest gameSessionRequest);
        RealityCheckCheckEnum RealityCheckCheck(int playerId);
        void RealityCheckResult(RealityCheckResultRequest realityCheckResultRequest);
        int Win(TransactionRequest transactionRequest);
        int Stake(TransactionRequest transactionRequest);
    }
}