﻿using System.Threading.Tasks;
using RGi.POC_Models.Enums;
using RGi.POC_Models.Requests;
using RGi.POC_Models.ViewModels;

namespace RGi.POC_Gateway.Interfaces
{
    public interface IOrleansPlayerController
    {
        Task<Player> Login(LoginRequest loginRequest);
        Task Logout(int playerId);
        Task<int> StartPlay(GameSessionStartRequest gameSessionRequest);
        Task StopPlay(GameSessionStopRequest gameSessionRequest);
        Task<RealityCheckCheckEnum> RealityCheckCheck(int playerId);
        Task RealityCheckResult(RealityCheckResultRequest realityCheckResultRequest);
        Task<int> Win(TransactionRequest transactionRequest);
        Task<int> Stake(TransactionRequest transactionRequest);
    }
}