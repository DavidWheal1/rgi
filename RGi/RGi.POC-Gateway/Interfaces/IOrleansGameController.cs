﻿using RGi.POC_Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RGi.POC_Gateway.Interfaces
{
    public interface IOrleansGameController
    {
        Task<List<Game>> GetGamesList();
        Task<string> GetGame(int gameId);
    }
}