﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using RGi.POC.OrleansDal;
using RGi.POC.OrleansDal.Common;
using RGi.POC.OrleansDal.Common.Interfaces;
using RGi.POC.OrleansDal.Interfaces;
using Serilog;
using System;
using System.IO;
using System.Threading.Tasks;
using RGi.POCGateway.PostSharp;

namespace RGi.POC_Gateway
{
    public class Startup
    {
        private ILoggerFactory _loggerFactory;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddSingleton(ClusterClientFactory.CreateClusterClientAsync());
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime applicationLifetime)
        {
            _loggerFactory = loggerFactory;
            _loggerFactory.AddFile(Configuration.GetSection("Logging"));
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            applicationLifetime.ApplicationStopped.Register(Log.CloseAndFlush);
        }

  
    }
}