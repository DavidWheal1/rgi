﻿namespace RGi.POC_Models.Enums
{
    public enum RealityCheckResultEnum
    {
        Continue,
        ViewHistory,
        Stop
    }
}