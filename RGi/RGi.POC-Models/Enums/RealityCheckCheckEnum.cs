﻿namespace RGi.POC_Models.Enums
{
    public enum RealityCheckCheckEnum
    {
        Ok,
        Expired,
        Unknown
    }
}