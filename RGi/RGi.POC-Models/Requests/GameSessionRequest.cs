﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace RGi.POC_Models.Requests
{
    public class GameSessionRequest
    {
        [JsonProperty("playerId")]
        [Required]
        public int PlayerId { get; set; }
        [JsonProperty("gameId")]
        [Required]
        public int GameId { get; set; }
    }
}