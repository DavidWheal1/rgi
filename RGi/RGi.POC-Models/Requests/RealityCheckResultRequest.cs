﻿using Newtonsoft.Json;
using RGi.POC_Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace RGi.POC_Models.Requests
{
    public class RealityCheckResultRequest
    {
        [JsonProperty("playerId")]
        [Required]
        public int PlayerId { get; set; }

        [JsonProperty("realityCheckResult")]
        [Required]
        public RealityCheckResultEnum RealityCheckResult { get; set; }
    }
}