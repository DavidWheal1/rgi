﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace RGi.POC_Models.Requests
{
    public class LoginRequest
    {
        [JsonProperty("username")]
        [Required]
        public string Username { get; set; }

        [JsonProperty("password")]
        [Required]
        public string Password { get; set; }
    }
}