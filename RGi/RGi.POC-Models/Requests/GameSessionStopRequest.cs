﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace RGi.POC_Models.Requests
{
    public class GameSessionStopRequest : GameSessionRequest
    {
        [JsonProperty("gameSessionId")]
        [Required]
        public int GameSessionId { get; set; }
    }
}