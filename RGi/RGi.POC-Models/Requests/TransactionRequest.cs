﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace RGi.POC_Models.Requests
{
    public class TransactionRequest
    {
        [JsonProperty("playerId")]
        [Required]
        public int PlayerId { get; set; }

        [JsonProperty("gameId")]
        [Required]
        public int GameId { get; set; }

        [JsonProperty("gameSessionId")]
        [Required]
        public int GameSessionId { get; set; }

        [JsonProperty("amount")]
        [Required]
        public decimal Amount { get; set; }
    }
}