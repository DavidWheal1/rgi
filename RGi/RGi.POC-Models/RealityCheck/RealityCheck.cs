﻿using System.Collections.Generic;

namespace RGi.POC_Models.RealityCheck
{
    public class RealityCheck
    {
        public int PlayerId { get; set; }
        public List<int> GameIds { get; set; } = new List<int>();
        public long Timer { get; set; }
    }
}