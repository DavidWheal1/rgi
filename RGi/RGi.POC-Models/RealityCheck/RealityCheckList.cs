﻿using System.Collections.Generic;

namespace RGi.POC_Models.RealityCheck
{
    public static class RealityCheckList
    {
        public static List<RealityCheck> RealityChecks { get; set; } = new List<RealityCheck>();

        public static void Add(RealityCheck realityCheck)
        {
            RealityChecks.Add(realityCheck);
        }

        public static void Remove(int playerId)
        {
            var realityCheck = RealityChecks.Find(rc => rc.PlayerId == playerId);

            if (realityCheck != null)
            {
                RealityChecks.Remove(realityCheck);
            }
        }

        public static RealityCheck Get(int playerId)
        {
            return RealityChecks.Find(rc => rc.PlayerId == playerId);
        }
    }
}