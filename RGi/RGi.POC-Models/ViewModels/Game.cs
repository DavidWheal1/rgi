﻿namespace RGi.POC_Models.ViewModels
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}