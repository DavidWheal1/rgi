﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RGi.POC_Entities
{
    public class Player
    {
        [Required]
        public int Id { get; set; }

        [Required, StringLength(10)]
        public string Barcode { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required, StringLength(50)]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        public bool IsLoggedIn { get; set; }
    }
}