﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RGi.POC_Entities
{
    public class Transaction
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int GameSessionId { get; set; }

        [Required]
        public int WalletId { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        public DateTime Started { get; set; }

        public DateTime Ended { get; set; }

        [Required]
        public int Count { get; set; }
    }
}