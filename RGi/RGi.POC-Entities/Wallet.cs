﻿using System.ComponentModel.DataAnnotations;

namespace RGi.POC_Entities
{
    public class Wallet
    {
        [Required]
        public int Id { get; set; }

        [Required, StringLength(50)]
        public string Name { get; set; }
    }
}