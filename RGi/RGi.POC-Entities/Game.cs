﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RGi.POC_Entities
{
    public class Game
    {
        [Required]
        public int Id { get; set; }

        [Required, StringLength(50)]
        public string Name { get; set; }

        [Required]
        public bool Enabled { get; set; }

        [Required]
        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }
    }
}