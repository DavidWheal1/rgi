﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RGi.POC_Entities
{
    public class GameSession
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int PlayerId { get; set; }

        [Required]
        public int GameId { get; set; }

        [Required]
        public DateTime Started { get; set; }

        public DateTime Updated { get; set; }

        [Required, StringLength(50)]
        public string Token { get; set; }

        [Required]
        public bool PlayerInSession { get; set; }

        [Required]
        public DateTime Expiry { get; set; }
    }
}