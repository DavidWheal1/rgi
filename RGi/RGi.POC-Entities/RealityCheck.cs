﻿using System.ComponentModel.DataAnnotations;

namespace RGi.POC_Entities
{
    public class RealityCheck
    {
        [Required]
        public int PlayerId { get; set; }
        [Required]
        public int GameId { get; set; }
        public long? Timer { get; set; }
    }
}