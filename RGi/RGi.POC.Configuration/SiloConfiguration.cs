﻿using System.Configuration;
using System.Net;

namespace RGi.POC.Config
{
    /// <summary>
    /// TODO: Make this read app.config etc
    /// </summary>
    public class SiloConfiguration
    {
        public string ClusterId { get; set; }
        public string ServiceId { get; set; }
        public string ClusterProviderConnectionString { get; set; }
        public string ClusterProviderInvariant { get; set; }
        public string GrainStorageName { get; set; }
        public IPAddress AdvertisedIpAddress { get; set; }
        public string ClusterServer { get; set; }

        public SiloConfiguration()
        {
            ClusterServer = ConfigurationManager.AppSettings["ClusterServer"];
            ClusterId = ConfigurationManager.AppSettings["ClusterId"];
            ServiceId = ConfigurationManager.AppSettings["ServiceId"];
            // MySql cluster provider seems not to work and fails with an invalid cast
            ClusterProviderConnectionString = ConfigurationManager.AppSettings["ClusterProviderConnectionString"];
            ClusterProviderInvariant = ConfigurationManager.AppSettings["ClusterProviderInvariant"];
            GrainStorageName = ConfigurationManager.AppSettings["GrainStorageName"]; ;
            AdvertisedIpAddress = IPAddress.Parse(ConfigurationManager.AppSettings[$"AdvertisedIPAddress{ClusterServer}"]);
        }
    }
}
