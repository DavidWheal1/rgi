﻿using RGi.POC.OrleansDal.Interfaces;
using RGi.POC_Entities;
using RGi.POC_Models.Requests;
using Serilog;
using System;

namespace RGi.POC.OrleansDal.BusinessLogic
{
    public static class TransactionHelper
    {
        public static int CreateTransaction(TransactionRequest transactionRequest, IUnitOfWork unitOfWork, ILogger logger)
        {
            try
            {
                var gameSession = unitOfWork.GameSessionRepository.Get(transactionRequest.GameSessionId);
                var transaction = new Transaction
                {
                    Amount = transactionRequest.Amount,
                    GameSessionId = gameSession.Id,
                    WalletId = 1 // hardcoded, for now
                };
                var transactionId = unitOfWork.TransactionRepository.Add(transaction);

                unitOfWork.Commit();
                return transactionId;
            }
            catch (Exception e)
            {
                logger.Error($"{e}");
                return -1;
            }
        }
    }
}