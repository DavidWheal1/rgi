﻿using System.Data;
using RGi.POC.OrleansDal.Common.Interfaces;

namespace RGi.POC.OrleansDal.DatabaseTypes.Interfaces
{
    public interface IDatabaseType
    {
        IDbConnection GetConnection(IConnectionStringProvider connectionStringProvider);
    }
}