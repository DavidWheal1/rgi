﻿namespace RGi.POC.OrleansDal.Interfaces
{
    public interface IRepositoryFactory
    {
        T GetRepository<T>() where T : class;
    }
}