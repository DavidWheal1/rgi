﻿using RGi.POC.OrleansDal.DatabaseTypes.Interfaces;

namespace RGi.POC.OrleansDal.Interfaces
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork CreateConnection<T>() where T : IDatabaseType, new();
    }
}
