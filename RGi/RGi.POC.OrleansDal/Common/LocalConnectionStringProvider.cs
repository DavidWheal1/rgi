﻿using System;
using System.Configuration;
using System.IO;
using RGi.POC.OrleansDal.Common.Interfaces;

namespace RGi.POC.OrleansDal.Common
{
    public class LocalConnectionStringProvider : IConnectionStringProvider
    {
        private string _connectionString { get; set; }

        public LocalConnectionStringProvider(string connectionString)
        {
            _connectionString = connectionString;
        }

        public string GetConnectionString(string databaseName = "pocorleans")
        {
            return _connectionString;
            //var connectionString = "server=poc-orleans.c1iqx3klgoev.eu-west-2.rds.amazonaws.com;database=pocorleans;user=orleans;password=a7ZcLQXCnDhp;Max Pool Size=10;Connection Lifetime=50;charset=utf8"; //ConfigurationManager.AppSettings["pocorleans"]; ;

            //if (string.IsNullOrEmpty(connectionString))
            //{
            //    throw new ArgumentNullException("connectionString", "Connection string is empty");
            //}

            //return connectionString;
        }
    }
}