﻿namespace RGi.POC.OrleansDal.Common.Interfaces
{
    public interface IConnectionStringProvider
    {
        string GetConnectionString(string databaseName = "pocorleans");
    }
}