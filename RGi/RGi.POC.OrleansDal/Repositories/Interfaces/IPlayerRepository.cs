﻿using RGi.POC_Entities;

namespace RGi.POC.OrleansDal.Repositories.Interfaces
{
    public interface IPlayerRepository
    {
        void Add(Player player);
        Player Get(int playerId);
        Player Login(string username, string password);
        void UpdateIsLoggedIn(int playerId, bool isLoggedIn);
    }
}