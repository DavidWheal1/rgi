﻿using RGi.POC_Entities;

namespace RGi.POC.OrleansDal.Repositories.Interfaces
{
    public interface IWalletRepository
    {
        void Add(Wallet wallet);
        Wallet Get(int walletId);
    }
}