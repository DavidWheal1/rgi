﻿using System.Collections.Generic;
using RGi.POC_Entities;

namespace RGi.POC.OrleansDal.Repositories.Interfaces
{
    public interface IRealityCheckRepository
    {
        void Add(RealityCheck realityCheck);
        IEnumerable<RealityCheck> Get(int playerId);
        void Remove(int playerId, int gameId);
        void Remove(int playerId);
        void UpdateTimer(RealityCheck realityCheck);
    }
}