﻿using System.Collections.Generic;
using RGi.POC_Entities;

namespace RGi.POC.OrleansDal.Repositories.Interfaces
{
    public interface IGameRepository
    {
        void Add(Game game);
        Game Get(int gameId);
        IEnumerable<Game> Get();
    }
}