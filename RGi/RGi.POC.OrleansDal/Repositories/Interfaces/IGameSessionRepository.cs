﻿using RGi.POC_Entities;

namespace RGi.POC.OrleansDal.Repositories.Interfaces
{
    public interface IGameSessionRepository
    {
        int Add(GameSession gameSession);
        GameSession Get(int gameSessionId);
        GameSession Get(int playerId, int gameId);
        void Update(GameSession gameSession);
        GameSession Get(string token);
    }
}