﻿using Dapper;
using RGi.POC.OrleansDal.Repositories.Interfaces;
using RGi.POC_Entities;
using System.Collections.Generic;
using System.Data;

namespace RGi.POC.OrleansDal.Repositories
{
    public class GameRepository : BaseRepository, IGameRepository
    {
        public GameRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public void Add(Game game)
        {
            var sql = @"
                INSERT INTO games(Name, Enabled, Created)
                VALUES (@name, @enabled, UTC_TIMESTAMP())";
            var args = new { name = game.Name, enabled = game.Enabled };

            Connection.Execute(sql, args, Transaction);
        }

        public Game Get(int gameId)
        {
            var sql = @"
                SELECT Id, Name, Enabled, Created, Updated
                FROM games
                WHERE Id = @gameId AND Enabled = 1";
            var args = new { gameId = gameId };

            return Connection.QuerySingleOrDefault<Game>(sql, args);
        }

        public IEnumerable<Game> Get()
        {
            var sql = @"
                SELECT Id, Name
                FROM games
                WHERE Enabled = 1";

            return Connection.Query<Game>(sql);
        }
    }
}