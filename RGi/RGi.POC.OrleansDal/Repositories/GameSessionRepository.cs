﻿using System;
using Dapper;
using RGi.POC.OrleansDal.Repositories.Interfaces;
using RGi.POC_Entities;
using System.Data;

namespace RGi.POC.OrleansDal.Repositories
{
    public class GameSessionRepository : BaseRepository, IGameSessionRepository
    {
        public GameSessionRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public int Add(GameSession gameSession)
        {
            int count = ReadCount();

            // Increment count
            count++;

            var sql = @"
                INSERT INTO game_sessions(PlayerId, GameId, Started, Token, PlayerInSession, Count)
                VALUES (@playerId, @gameId, UTC_TIMESTAMP(), @token, @playerInSession, @count);
                SELECT LAST_INSERT_ID();";
            var args = new { playerId = gameSession.PlayerId, gameId = gameSession.GameId, token = gameSession.Token, playerInSession = gameSession.PlayerInSession, count = count };
            var gameSessionId = Connection.QuerySingle<int>(sql, args, Transaction);

            InsertDummy(count, gameSessionId);
            UpdateCount(count);
            return gameSessionId;
        }

        public GameSession Get(int gameSessionId)
        {
            var sql = @"
                SELECT Id, PlayerId, GameId, Started, Updated, Token, PlayerInSession, Expiry
                FROM game_sessions
                WHERE Id = @gameSessionId";
            var args = new { gameSessionId = gameSessionId };

            return Connection.QuerySingleOrDefault<GameSession>(sql, args);
        }

        public GameSession Get(string token)
        {
            var sql = @"
                SELECT Id, PlayerId, GameId, Started, Updated, Token, PlayerInSession, Expiry
                FROM game_sessions
                WHERE Token = @token and PlayerInSession=1";
            var args = new { token = token };

            return Connection.QuerySingleOrDefault<GameSession>(sql, args);
        }

        public GameSession Get(int playerId, int gameId)
        {
            var sql = @"
                SELECT Id, PlayerId, GameId, Started, Updated, Token, PlayerInSession, Expiry
                FROM game_sessions
                WHERE PlayerId = @playerId AND GameId = @gameId AND PlayerInSession = 1";
            var args = new { playerId = playerId, gameId = gameId };

            return Connection.QuerySingleOrDefault<GameSession>(sql, args);
        }

        public void Update(GameSession gameSession)
        {
            int count = ReadCount();

            // Increment count
            count++;

            var sql = @"
                UPDATE game_sessions
                SET Updated = @updated, Token = NULL, PlayerInSession = false, Count = @count
                WHERE Id = @gameSessionId;";
            var args = new { updated = gameSession.Updated, count = count, gameSessionId = gameSession.Id };

            Connection.Execute(sql, args, Transaction);
            InsertDummy(count, gameSession.Id);
            UpdateCount(count);
        }
    }
}