﻿using RGi.POC.OrleansDal.Repositories.Interfaces;
using System.Data;
using Dapper;
using RGi.POC_Entities;

namespace RGi.POC.OrleansDal.Repositories
{
    public class TransactionRepository : BaseRepository, ITransactionRepository
    {
        public TransactionRepository(IDbTransaction dbTransaction)
            : base(dbTransaction)
        {
        }

        public int Add(Transaction transaction)
        {
            int count = ReadCount();

            // Increment count
            count++;

            var sql = @"
                INSERT INTO transactions(GameSessionId, WalletId, Amount, Started,Ended, Count)
                VALUES (@gameSessionId, @walletId, @amount, UTC_TIMESTAMP(),UTC_TIMESTAMP(), @count);
                SELECT LAST_INSERT_ID();";
            var args = new { gameSessionId = transaction.GameSessionId, walletId = transaction.WalletId, amount = transaction.Amount, count = count };
            var transactionId = Connection.QuerySingle<int>(sql, args, Transaction);

            InsertDummy(count, transactionId);
            UpdateCount(count);
            return transactionId;
        }

        public Transaction Get(int transactionId)
        {
            var sql = @"
                SELECT Id, GameSessionId, WalletId, Amount, Started, Ended
                FROM transactions
                WHERE Id = @transactionId";
            var args = new { transactionId = transactionId };

            return Connection.QuerySingleOrDefault<Transaction>(sql, args);
        }
    }
}