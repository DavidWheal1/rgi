﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Runtime;
using RGi.POC.GrainInterfaces.Interfaces;
using RGi.POC_Models.Requests;

namespace RGi.POC.BackOfficeTester
{
    public partial class Form1 : Form
    {
        private IClusterClient startupTask;
        public Form1()
        {
            InitializeComponent();

        }

     

        private static async Task<IClusterClient> StartAdoClusterClientWithRetries(int initializeAttemptsBeforeFailing = 5)
        {
            int attempt = 0;
            IClusterClient client;
            while (true)
            {
                try
                {
#if DEBUG
                    client = new ClientBuilder()
                        .UseLocalhostClustering()
                        .Configure<ClusterOptions>(options =>
                        {
                            options.ClusterId = ConfigurationManager.AppSettings["ClusterId"];
                            options.ServiceId = ConfigurationManager.AppSettings["ServiceId"];
                        })
                        .ConfigureLogging(logging => logging.AddConsole())

                        .Build();
#else
                    client = new ClientBuilder()
                         .UseAdoNetClustering(options =>
                         {
                             options.Invariant = ConfigurationManager.AppSettings["ClusterProviderInvariant"];
                             options.ConnectionString = ConfigurationManager.AppSettings["ClusterProviderConnectionString"];
                         })
                         .Configure<ClusterOptions>(options =>
                         {
                             options.ClusterId = ConfigurationManager.AppSettings["ClusterId"];
                             options.ServiceId = ConfigurationManager.AppSettings["ServiceId"];
                         })
                         .ConfigureLogging(logging => logging.AddConsole())

                         .Build();
#endif


                    client.Connect(async ex =>
                    {
                        Console.WriteLine(ex);
                        Console.WriteLine("Retrying...");
                        await Task.Delay(3000);
                        return true;
                    }).Wait();
                    MessageBox.Show("Client successfully connect to silo host", "Start up");
                    break;
                }
                catch (SiloUnavailableException)
                {
                    attempt++;
                    Console.WriteLine($"Attempt {attempt} of {initializeAttemptsBeforeFailing} failed to initialize the (Ado) Orleans client.");
                    if (attempt > initializeAttemptsBeforeFailing)
                    {
                        throw;
                    }
                    await Task.Delay(TimeSpan.FromSeconds(4));
                }
            }

            return client;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private async void btnStarted_Click(object sender, EventArgs e)
        {
            startupTask = await StartAdoClusterClientWithRetries();

            //if (startupTask.IsCompleted)
            //{
            //    MessageBox.Show("Yes", "Silo initialised");
            //}
            //else
            //{
            //    MessageBox.Show("No", "Silo initialised");

            //}
        }

        private async void btnStartCreatingPlayers_Click(object sender, EventArgs e)
        {

            try
            {
                DateTime starttime = DateTime.UtcNow;
                var g = startupTask.GetGrain<IAccessManager>(Guid.Empty);
                int max = Convert.ToInt32(txtNumberOfPlayers.Text);
                for (int i = 0; i < max; i++)
                {
                    var playerGrain =
                        await g.Login(new LoginRequest() {Username = $"marco{i}", Password = $"password{i}"});
                    //var loggedIn = await playerGrain.IsLoggedIn();
                }

                var endtime = DateTime.UtcNow;
                txtCreatedPlayersTime.Text =
                    endtime.Subtract(starttime).TotalSeconds.ToString(CultureInfo.InvariantCulture);
            }
            catch (SiloUnavailableException exception)
            {
                MessageBox.Show(exception.Message, $"{exception.GetType()} No silo available");
            }
            catch (OrleansMessageRejectionException exception)
            {
                MessageBox.Show(exception.Message, $"{exception.GetType()} Silo gone down");
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, $"{exception.GetType()} Failed");

            }
        }

       
    }
}

