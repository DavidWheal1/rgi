﻿namespace RGi.POC.BackOfficeTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStarted = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCreatedPlayersTime = new System.Windows.Forms.TextBox();
            this.btnStartCreatingPlayers = new System.Windows.Forms.Button();
            this.txtNumberOfPlayers = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStarted
            // 
            this.btnStarted.Location = new System.Drawing.Point(33, 23);
            this.btnStarted.Name = "btnStarted";
            this.btnStarted.Size = new System.Drawing.Size(70, 39);
            this.btnStarted.TabIndex = 0;
            this.btnStarted.Text = "Start";
            this.btnStarted.UseVisualStyleBackColor = true;
            this.btnStarted.Click += new System.EventHandler(this.btnStarted_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(156, 23);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(792, 388);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtCreatedPlayersTime);
            this.tabPage1.Controls.Add(this.btnStartCreatingPlayers);
            this.tabPage1.Controls.Add(this.txtNumberOfPlayers);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(784, 362);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Create Player Grains";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(88, 195);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Time taken (s)";
            // 
            // txtCreatedPlayersTime
            // 
            this.txtCreatedPlayersTime.Location = new System.Drawing.Point(196, 192);
            this.txtCreatedPlayersTime.Name = "txtCreatedPlayersTime";
            this.txtCreatedPlayersTime.Size = new System.Drawing.Size(167, 20);
            this.txtCreatedPlayersTime.TabIndex = 3;
            // 
            // btnStartCreatingPlayers
            // 
            this.btnStartCreatingPlayers.Location = new System.Drawing.Point(187, 112);
            this.btnStartCreatingPlayers.Name = "btnStartCreatingPlayers";
            this.btnStartCreatingPlayers.Size = new System.Drawing.Size(94, 32);
            this.btnStartCreatingPlayers.TabIndex = 2;
            this.btnStartCreatingPlayers.Text = "Go";
            this.btnStartCreatingPlayers.UseVisualStyleBackColor = true;
            this.btnStartCreatingPlayers.Click += new System.EventHandler(this.btnStartCreatingPlayers_Click);
            // 
            // txtNumberOfPlayers
            // 
            this.txtNumberOfPlayers.Location = new System.Drawing.Point(157, 41);
            this.txtNumberOfPlayers.Name = "txtNumberOfPlayers";
            this.txtNumberOfPlayers.Size = new System.Drawing.Size(94, 20);
            this.txtNumberOfPlayers.TabIndex = 1;
            this.txtNumberOfPlayers.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Number to Create";
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(784, 362);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Failover";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 450);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnStarted);
            this.Name = "Form1";
            this.Text = "Orleans PoC";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStarted;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCreatedPlayersTime;
        private System.Windows.Forms.Button btnStartCreatingPlayers;
        private System.Windows.Forms.TextBox txtNumberOfPlayers;
        private System.Windows.Forms.Label label1;
    }
}

